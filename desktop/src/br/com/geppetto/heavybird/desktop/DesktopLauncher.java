package br.com.geppetto.heavybird.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import br.com.geppetto.heavybird.HeavyBirdMain;

public class DesktopLauncher {
	private static final boolean REBUILD_ATLAS = false;
	private static final boolean DRAW_DEBUG_LINES = false;
	public static void main (String[] arg) {
		if(REBUILD_ATLAS) {
			Settings settings = new Settings();
			settings.pot = true;
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.paddingX = 2;
			settings.paddingY = 2;
			settings.debug = DRAW_DEBUG_LINES;
			//settings.filterMag = TextureFilter.Linear;
			//settings.filterMin = TextureFilter.Linear;
			
			TexturePacker.process(settings, "assets-raw/images",
					"/assetsToHeavyBird/images", "heavyBird.pack");
			
			System.out.print("DONE!");
		} else {
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			config.title = "Heavy Anchor";
			//config.width = 200;
			config.width = 339;
			config.height = 508;
			new LwjglApplication(new HeavyBirdMain(), config);
		}
	}
}
