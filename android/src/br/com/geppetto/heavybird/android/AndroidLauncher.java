package br.com.geppetto.heavybird.android;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import br.com.geppetto.heavybird.HeavyBirdMain;
import br.com.geppetto.heavybird.ads.IActivityRequestHandler;
import br.com.geppetto.heavybird.controller.Mode;
import br.com.geppetto.heavybird.google.IGoogleServices;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;

public class AndroidLauncher extends AndroidApplication implements IActivityRequestHandler, IGoogleServices {
	private static final String TAG = AndroidLauncher.class.getSimpleName();
	private static final int REQUEST_CODE_UNUSED = 9002;
	
	private GameHelper _gameHelper;
	
	private static final int SHOW_BOTTOM_ADS = 0;
    private static final int HIDE_BOTTOM_ADS = 1;
    private static final int SHOW_FULLSCREEN_AD_MENU = 2;
    private static final int SHOW_FULLSCREEN_AD_ON_GAME = 3;
    private static final int SUBMIT_SCORE = 4;
    private static final int SHOW_SCORES = 5;
    //private boolean showBannerTop;
    //private boolean showBannerBottom;

    private RelativeLayout relativeLayout;
    
	private AdView bannerBottom;
	
	private InterstitialAd fullScreenAdMenu;
	private InterstitialAd fullScreenAdOnGame;
	
	private AdRequest adRequest;

    private Handler handler = new AdsHandler();
    
    private AdSize adSize;
    
    private boolean bannerIsOn;
    
    private boolean mustSubmitScore;
    private Mode currMode;
    private long currScore;
    
    private boolean mustShowScore;
	
	private void initGame(RelativeLayout layout) {
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		View gameView = initializeForView(new HeavyBirdMain(this, this), cfg);
		layout.addView(gameView);
	}
	
	private InterstitialAd getInterstitialAd(String code) {
		
		final InterstitialAd ia = new InterstitialAd(this);
		ia.setAdUnitId(code);
		ia.loadAd(adRequest);
		/*ia.setAdListener(new AdListener() {
        	@Override
        	public void onAdFailedToLoad(int errorCode) {
        		new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(30000);
							if(!ia.isLoaded())
								ia.loadAd(adRequest);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
        	}
		});*/
		return ia;
	}
	
	private void initFullScreenAdMenu() {
		fullScreenAdMenu = getInterstitialAd("ca-app-pub-2213353310088719/3887416585");
	}
	
	private void initFullScreenAdOnGame() {
		fullScreenAdOnGame = getInterstitialAd("ca-app-pub-2213353310088719/5364149780");
	}
	
	private void initBannerBottom() {
		 // Criar o adView.
        AdView adView = new AdView(this);
        //adView.setAdUnitId("ca-app-pub-4007764316542907/3690770075");
        adView.setAdUnitId("ca-app-pub-2213353310088719/2550284182"); //1
        //adView.setAdUnitId("ca-app-pub-4007764316542907/8591974475"); //2
        //adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdSize(adSize);
        // Iniciar uma solicita��o gen�rica.
        
        //Carregar o adView com a solicita��o de an�ncio.
        adView.loadAd(adRequest);
      
        bannerBottom = adView;
        /*bannerBottom.setAdListener(new AdListener() {
        	@Override
        	public void onAdFailedToLoad(int errorCode) {
        		Log.d(TAG, "Banner failed");
        		new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(30000);
							bannerBottom.loadAd(adRequest);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
        	}
		});*/
        addBannerBottomView();
	}
	
	private void addBannerBottomView() {
		RelativeLayout.LayoutParams adParams = 
        		new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
        				RelativeLayout.LayoutParams.WRAP_CONTENT);
        //adParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        //adView.setVisibility(View.GONE);
        relativeLayout.addView(bannerBottom, adParams);
        bannerIsOn = true;
	}
	
	private void removeBannerBottomView() {
		relativeLayout.removeView(bannerBottom);
		bannerIsOn = false;
	}
	
	@SuppressWarnings("deprecation")
	private void initAdSize() {
		AdSize adSize = AdSize.SMART_BANNER;
		
		int width = 0;
		int height = 0;
		Point size = new Point();
		WindowManager w = getWindowManager();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
		    w.getDefaultDisplay().getSize(size);

		    width = size.x;
		    height = size.y;
		} else {
		    Display d = w.getDefaultDisplay();
		    width = d.getWidth();
		    height = d.getHeight();
		}
		
		if(width >= 728 && height >= 90 ) {
			adSize = AdSize.LEADERBOARD;
			//System.out.println("728 x 90");
		} else if (width >= 468 && height >= 60 ) {
			adSize = AdSize.FULL_BANNER;
			//System.out.println("468 x 60");
		} else if (width >= 320 && height >= 50 ) {
			adSize = AdSize.BANNER;
			//System.out.println("320 x 50");
		}
		this.adSize = adSize;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG, "Creating it");
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        
        // Google!-------
        
        // Create the GameHelper.
        _gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
        _gameHelper.enableDebugLog(false);

        GameHelperListener gameHelperListener = 
        		new GameHelper.GameHelperListener() {
	        @Override
	        public void onSignInSucceeded()
	        {
	        	if(mustSubmitScore) {
	        		submitScore2();
	        		mustSubmitScore = false;
	        	}
	        	if(mustShowScore) {
	        		showScores2();
	        		mustShowScore = false;
	        	}
	        }
	
	        @Override
	        public void onSignInFailed()
	        {
	        	Log.d(TAG, "SignInFailed");
	        }
        };
        _gameHelper.setMaxAutoSignInAttempts(0);
        _gameHelper.setup(gameHelperListener);
        
        //---------------
        
        relativeLayout = new RelativeLayout(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        		WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        
        initGame(relativeLayout);

        adRequest = new AdRequest.Builder()
        //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addKeyword("children")
        .addKeyword("games")
        .addKeyword("cartoons")
        .addKeyword("toys")
        
        //.addTestDevice("55FFA30161CF5F49F8F3753A53043997") // Ettore
        //.addTestDevice("D6AB0850E61017E4947E7354C1829745") // Meu
        //.addTestDevice("77A5D9844A99F5892F6645779F8203D7") //Matheus
        //.addTestDevice("44D0948B0DCB40FE76E4D99C25F561FA") // Marisa
        .build();
        
        initAdSize();
        
        initBannerBottom();
        
        initFullScreenAdMenu();
        
        initFullScreenAdOnGame();
        
        setContentView(relativeLayout);
    }

	@Override
	public void showFooterAds(boolean show) {
		 handler.sendEmptyMessage(show ? SHOW_BOTTOM_ADS : HIDE_BOTTOM_ADS);
	}

	@Override
	public void showFullScreenAdMenu() {
		handler.sendEmptyMessage(SHOW_FULLSCREEN_AD_MENU);
	}
	
	@Override
	public void showFullScreenAdOnGame() {
		handler.sendEmptyMessage(SHOW_FULLSCREEN_AD_ON_GAME);
	}
	
	private class AdsHandler extends Handler {
		@Override
	     public void handleMessage(Message msg) {
	         switch(msg.what) {
	             case SHOW_BOTTOM_ADS:
	             synchronized (bannerBottom) {
	            	 if(!bannerIsOn) {
		            	 bannerBottom.resume();
		                 addBannerBottomView();
	            	 }
	                 break;
	             }
	             case HIDE_BOTTOM_ADS:
	             synchronized (bannerBottom) {
	            	 if(bannerIsOn) {
	            		bannerBottom.pause();
	            		removeBannerBottomView();
	            	 }
	                 break;
	             }
	             case SHOW_FULLSCREEN_AD_MENU:
	             {
	             	if (fullScreenAdMenu.isLoaded()) {
	             		fullScreenAdMenu.show();
	             		initFullScreenAdMenu();
	             	}
	             	
	             	break;
	             }
	             case SHOW_FULLSCREEN_AD_ON_GAME:
	             {
	            	 if(fullScreenAdOnGame.isLoaded()) {
	             		fullScreenAdOnGame.show();
	             		initFullScreenAdOnGame();
		             }
	            	 break;
	             }
	             case SUBMIT_SCORE:
	             {
	            	 if (isSignedIn()) {
	         			submitScore2();
	         		} else {
	         		//	// Maybe sign in here then redirect to submitting score?
	         			mustSubmitScore = true;
	         		//	signIn();
	         		}
	            	 break;
	             }
	             case SHOW_SCORES:
	             {
	            	 if (isSignedIn()) {
	         			showScores2();
	         		} else {
	         		//	// Maybe sign in here then redirect to showing scores?
	         			mustShowScore = true;
	         		//	signIn();
	         		}
	            	 break;
	             }
	             default : assert false : "Invalid option";
	         }
	     }
	}
	
	@Override
	protected void onResume() {
		if(bannerBottom != null && bannerIsOn) bannerBottom.resume();
		if(fullScreenAdMenu != null && !fullScreenAdMenu.isLoaded())
			initFullScreenAdMenu();
		if(fullScreenAdOnGame != null && !fullScreenAdOnGame.isLoaded())
			initFullScreenAdOnGame();
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		if (bannerBottom != null) bannerBottom.pause();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		if(bannerBottom != null) bannerBottom.destroy();
		_gameHelper.disconnect();
		super.onDestroy();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		_gameHelper.onStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		_gameHelper.onStop();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		_gameHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void signIn() {
		try {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					_gameHelper.beginUserInitiatedSignIn();
				}
			});
		}
		catch (Exception e) {
			Gdx.app.log("MainActivity", "Log in failed: " + e.getMessage() + ".");
		}
	}

	@Override
	public void signOut() {
		try {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					_gameHelper.signOut();
				}
			});
		}
		catch (Exception e) {
			Gdx.app.log("MainActivity", "Log out failed: " + e.getMessage() + ".");
		}
	}

	@Override
	public void rateGame() {
		// Replace the end of the URL with the package of your game
		String str = "https://play.google.com/store/apps/details?id=br.com.geppetto.heavybird.android";
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));
	}
	
	private void submitScore2() {
		switch(currMode) {
		case EASY:
			Games.Leaderboards.submitScore(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_takeiteasy), currScore);
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_takeiteasy)), REQUEST_CODE_UNUSED);
			break;
		case HARDCORE:
			Games.Leaderboards.submitScore(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_hardcore), currScore);
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_hardcore)), REQUEST_CODE_UNUSED);
			break;
		}
	}

	@Override
	public void submitScore(Mode mode, long score) {
		this.currMode = mode;
		currScore = score;
		handler.sendEmptyMessage(SUBMIT_SCORE);
	}
	
	private void showScores2() {
		switch(currMode) {
		case EASY:
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_takeiteasy)), REQUEST_CODE_UNUSED);
			break;
		case HARDCORE:
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_id_hardcore)), REQUEST_CODE_UNUSED);
			break;
		}
	}

	@Override
	public void showScores(Mode mode) {
		this.currMode = mode;
		handler.sendEmptyMessage(SHOW_SCORES);
	}

	@Override
	public boolean isSignedIn() {
		return _gameHelper.isSignedIn();
	}
}