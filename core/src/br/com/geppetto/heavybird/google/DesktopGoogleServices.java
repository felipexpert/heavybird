package br.com.geppetto.heavybird.google;

import br.com.geppetto.heavybird.controller.Mode;

public class DesktopGoogleServices implements IGoogleServices {
	@Override
	public void signIn() {
		System.out.println("DesktopGoogleServies: signIn()");
	}

	@Override
	public void signOut() {
		System.out.println("DesktopGoogleServies: signOut()");
	}

	@Override
	public void rateGame() {
		System.out.println("DesktopGoogleServices: rateGame()");
	}

	@Override
	public void submitScore(Mode mode, long score) {
		System.out.println("DesktopGoogleServies: submitScore(" + score + ") " + mode);
	}

	@Override
	public void showScores(Mode mode) {
		System.out.println("DesktopGoogleServies: showScores() " + mode);
	}

	@Override
	public boolean isSignedIn() {
		System.out.println("DesktopGoogleServies: isSignedIn()");
		return false;
	}
}
