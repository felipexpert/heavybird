package br.com.geppetto.heavybird.google;

import br.com.geppetto.heavybird.controller.Mode;

public interface IGoogleServices {
	public void signIn();

	public void signOut();

	public void rateGame();

	public void submitScore(Mode mode, long score);

	public void showScores(Mode mode);

	public boolean isSignedIn();
}
