package br.com.geppetto.heavybird;

import br.com.geppetto.heavybird.ads.External;
import br.com.geppetto.heavybird.ads.IActivityRequestHandler;
import br.com.geppetto.heavybird.ads.NullActivityRequestHandler;
import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.google.DesktopGoogleServices;
import br.com.geppetto.heavybird.google.IGoogleServices;
import br.com.geppetto.heavybird.persistence.LocalPersistence;
import br.com.geppetto.heavybird.screen.DirectedGame;
import br.com.geppetto.heavybird.screen.manager.ScreenManager;
import br.com.geppetto.heavybird.screen.manager.ScreenType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

public class HeavyBirdMain extends DirectedGame {
	
	public HeavyBirdMain() {
		External.instance.handler = new NullActivityRequestHandler();
		External.instance.iGoogleServices = new DesktopGoogleServices();
	}
	
	public HeavyBirdMain(IActivityRequestHandler handler, IGoogleServices iGoogleServices) {
		External.instance.handler = handler;
		External.instance.iGoogleServices = iGoogleServices;
	}
	
	@Override
	public void create () {
		// Set Libgdx log level
		//Gdx.app.setLogLevel(Application.LOG_DEBUG);
		Gdx.input.setCatchBackKey(true);
		// Load Skin Preferences;
		LocalPersistence.instance.loadSkin();
		// Load assets
		Assets.instance.init(new AssetManager());
		// Load preferences for audio settings and start playing music
		//GamePreferences.instance.load();
		//AudioManager.instance.play(Assets.instance.music.song01);

		// Start game at menu screen
		//ScreenTransition transition = ScreenTransitionSlice.init(2, ScreenTransitionSlice.UP_DOWN, 10, Interpolation.pow5Out);
		ScreenManager.instance.init(this);
		ScreenManager.instance.setScreen(ScreenType.MENU);
	}
	
}
