package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class RegionWrapper {
	private AtlasRegion region;
	
	void setRegion(AtlasRegion region) {
		this.region = region;
	}
	
	public AtlasRegion getRegion() {
		return region;
	}
}
