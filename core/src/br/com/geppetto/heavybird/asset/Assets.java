package br.com.geppetto.heavybird.asset;

import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

public class Assets implements Disposable, AssetErrorListener{
private static final String TAG = Assets.class.getName();
	
	private AssetManager assetManager;
	public static final Assets instance = new Assets();
	
	public final Bird bird;
	public final Anchor anchor;
	public final Pipe pipe;
	public final Wall wall;
	public final Digits digits;
	public final AssetSounds sounds;
	public final AssetMusics musics;
	public final MenuItem menuItem;
	public final Cloud cloud;
	
	private TextureAtlas atlas;
	
	private Assets() {
		bird = new Bird();
		anchor = new Anchor();
		pipe = new Pipe();
		wall = new Wall();
		digits = new Digits();
		sounds = new AssetSounds();
		musics = new AssetMusics();
		menuItem = new MenuItem();
		cloud = new Cloud();
	}
	
	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
		assetManager.setErrorListener(this);
		assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);
		assetManager.load("sounds/wing.wav", Sound.class);
		assetManager.load("sounds/wingSpeedy.wav", Sound.class);
		assetManager.load("sounds/point.wav", Sound.class);
		assetManager.load("sounds/hit.wav", Sound.class);
		assetManager.load("sounds/falling.wav", Sound.class);
		assetManager.load("sounds/play.wav", Sound.class);
		assetManager.load("sounds/quit.wav", Sound.class);
		assetManager.load("sounds/pipeColision.wav", Sound.class);
		assetManager.load("musics/theme.mp3", Music.class);
		assetManager.finishLoading();
		
		Gdx.app.debug(TAG, "assets loaded: " + assetManager.getAssetNames().size);
		for(String name : assetManager.getAssetNames()) {
			Gdx.app.debug(TAG, "asset: " + name);
		}
		
		atlas = new TextureAtlas(Constants.TEXTURE_ATLAS_OBJECTS);
				
		bird.init(atlas);
		anchor.init(atlas);
		pipe.init(atlas);
		wall.init(atlas);
		digits.init(atlas);
		sounds.init(assetManager);
		musics.init(assetManager);
		menuItem.init(atlas);
		cloud.init(atlas);
	}
	
	public void reloadSkins() {
		bird.init(atlas);
		anchor.init(atlas);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void error(AssetDescriptor asset, Throwable throwable) {
		Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception)throwable);
	}

	@Override
	public void dispose() {
		assetManager.dispose();
	}
}
