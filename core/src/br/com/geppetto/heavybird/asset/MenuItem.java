package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class MenuItem {
	private static final float HAND_CLICK_ON = .25f;
	public final RegionWrapper easyWrapper = new RegionWrapper();
	public final RegionWrapper hardcoreWrapper = new RegionWrapper();
	public final RegionWrapper okWrapper = new RegionWrapper();
	public final RegionWrapper soundWrapper = new RegionWrapper();
	public final RegionWrapper scoreWrapper = new RegionWrapper();
	public final RegionWrapper bestWrapper = new RegionWrapper();
	public final RegionWrapper scoreBackgroundWrapper = new RegionWrapper();
	public final RegionWrapper medalPlatinumWrapper = new RegionWrapper();
	public final RegionWrapper medalGoldWrapper = new RegionWrapper();
	public final RegionWrapper medalSilverWrapper = new RegionWrapper();
	public final RegionWrapper medalBronzeWrapper = new RegionWrapper();
	public final RegionWrapper resumeWrapper = new RegionWrapper();
	public final RegionWrapper exitWrapper = new RegionWrapper();
	public final RegionWrapper arrowWrapper = new RegionWrapper();
	public final AnimationWrapper handRightWrapper = new AnimationWrapper();
	public final AnimationWrapper handLeftWrapper = new AnimationWrapper();
	public final RegionWrapper getReadyWrapper = new RegionWrapper();
	public final RegionWrapper gameOverWrapper = new RegionWrapper();
	public final RegionWrapper groundGreen = new RegionWrapper();
	public final RegionWrapper groundBrown = new RegionWrapper();
	public final RegionWrapper leaderBoard = new RegionWrapper();
	public final RegionWrapper share = new RegionWrapper();
	public final RegionWrapper rate = new RegionWrapper();
	
	MenuItem() {}
	
	void init(TextureAtlas atlas) {
		easyWrapper.setRegion(atlas.findRegion("easy"));
		hardcoreWrapper.setRegion(atlas.findRegion("hardcore"));
		okWrapper.setRegion(atlas.findRegion("ok"));
		soundWrapper.setRegion(atlas.findRegion("sound"));
		scoreWrapper.setRegion(atlas.findRegion("score"));
		bestWrapper.setRegion(atlas.findRegion("best"));
		scoreBackgroundWrapper.setRegion(atlas.findRegion("scoreBackground"));
		scoreBackgroundWrapper.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		medalPlatinumWrapper.setRegion(atlas.findRegion("medalPlatinum"));
		medalGoldWrapper.setRegion(atlas.findRegion("medalGold"));
		medalSilverWrapper.setRegion(atlas.findRegion("medalSilver"));
		medalBronzeWrapper.setRegion(atlas.findRegion("medalBronze"));
		resumeWrapper.setRegion(atlas.findRegion("resume"));
		exitWrapper.setRegion(atlas.findRegion("exit"));
		arrowWrapper.setRegion(atlas.findRegion("arrow"));
		AtlasRegion hand = atlas.findRegion("hand");
		AtlasRegion handRight = atlas.findRegion("handRight"); 
		AtlasRegion handLeft = atlas.findRegion("handLeft");
		Array<AtlasRegion> hr = new Array<AtlasRegion>();
		Array<AtlasRegion> hl = new Array<AtlasRegion>();
		
		hr.add(hand);
		hr.add(handRight);
		hl.add(hand);
		hl.add(handLeft);
		
		handRightWrapper.setAnimation(new Animation(HAND_CLICK_ON, hr));
		handLeftWrapper.setAnimation(new Animation(HAND_CLICK_ON, hl));
		
		getReadyWrapper.setRegion(atlas.findRegion("getReady"));
		gameOverWrapper.setRegion(atlas.findRegion("gameOver"));
		groundGreen.setRegion(atlas.findRegion("groundGreen"));
		groundBrown.setRegion(atlas.findRegion("groundBrown"));
		leaderBoard.setRegion(atlas.findRegion("leaderboard"));
		share.setRegion(atlas.findRegion("share"));
		rate.setRegion(atlas.findRegion("rate"));
	}
}
