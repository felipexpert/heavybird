package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;

public class AssetMusics {
	public Music theme;
	
	AssetMusics() {}
	
	void init(AssetManager manager) {
		theme = manager.get("musics/theme.mp3", Music.class);
	}
}
