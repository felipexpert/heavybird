package br.com.geppetto.heavybird.asset;

import br.com.geppetto.heavybird.persistence.LocalPersistence;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Anchor {
	public final RegionWrapper anchorWrapper = new RegionWrapper();
	
	Anchor() {}
	
	void init(TextureAtlas atlas) {
		int as = LocalPersistence.instance.getAnchorSkin();
		anchorWrapper.setRegion(atlas.findRegion("anchor" + as));
	}
}
