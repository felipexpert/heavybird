package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

public class AssetSounds {
	public Sound wing;
	public Sound wingSpeedy;
	public Sound point;
	public Sound hit;
	public Sound falling;
	public Sound play;
	public Sound quit;
	public Sound pipeColision;
	
	AssetSounds() {}
	
	void init(AssetManager manager) {
		wing = manager.get("sounds/wing.wav", Sound.class);
		wingSpeedy = manager.get("sounds/wingSpeedy.wav", Sound.class);
		point = manager.get("sounds/point.wav", Sound.class);
		hit = manager.get("sounds/hit.wav", Sound.class);
		falling = manager.get("sounds/falling.wav", Sound.class);
		play = manager.get("sounds/play.wav", Sound.class);
		quit = manager.get("sounds/quit.wav", Sound.class);
		pipeColision = manager.get("sounds/pipeColision.wav", Sound.class);
	}
}
