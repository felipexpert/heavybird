package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.g2d.Animation;

public class AnimationWrapper {
	private Animation animation;
	
	void setAnimation(Animation animation) {
		this.animation = animation;
	}
	
	public Animation getAnimation() {
		return animation;
	}
}
