package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Pipe {
	public final RegionWrapper pipeWrapper = new RegionWrapper();
	public final RegionWrapper pipeEdgeWrapper = new RegionWrapper();
	
	Pipe() {}
	
	void init(TextureAtlas atlas) {
		pipeWrapper.setRegion(atlas.findRegion("pipe"));
		pipeEdgeWrapper.setRegion(atlas.findRegion("pipeEdge"));
	}
}
