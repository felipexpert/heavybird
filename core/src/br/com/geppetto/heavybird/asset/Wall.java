package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Wall {
	public final RegionWrapper wallWrapper = new RegionWrapper();
	
	Wall() {}
	
	void init(TextureAtlas atlas) {
		wallWrapper.setRegion(atlas.findRegion("wall"));
	}
}
