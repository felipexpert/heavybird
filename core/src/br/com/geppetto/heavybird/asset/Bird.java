package br.com.geppetto.heavybird.asset;

import br.com.geppetto.heavybird.persistence.LocalPersistence;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class Bird {
	public final RegionWrapper birdWrapper = new RegionWrapper();
	public final AnimationWrapper birdFlyWrapper = new AnimationWrapper();
	public final AnimationWrapper birdFallWrapper = new AnimationWrapper();
	public final RegionWrapper birdSpeedyWrapper = new RegionWrapper();
	public final AnimationWrapper birdMenuWrapper = new AnimationWrapper();
	public final RegionWrapper logoWrapper = new RegionWrapper();
	
	Bird() {}
	
	void init(TextureAtlas atlas) {
		int bs = LocalPersistence.instance.getBirdSkin();
		{
			Array<AtlasRegion> bFly = atlas.findRegions("birdFly" + bs);
			birdFlyWrapper.setAnimation(new Animation(1f / 16f, bFly));
			birdMenuWrapper.setAnimation(new Animation(1f / 12f, bFly));
			birdMenuWrapper.getAnimation().setPlayMode(PlayMode.LOOP);
			birdWrapper.setRegion(bFly.get(0));
		}
		{
			Array<AtlasRegion> bFall = atlas.findRegions("birdFall" + bs);
			birdFallWrapper.setAnimation(new Animation(1f / 12f, bFall));
			birdFallWrapper.getAnimation().setPlayMode(PlayMode.LOOP);
		}
		birdSpeedyWrapper.setRegion(atlas.findRegion("birdSpeedy" + bs));
		logoWrapper.setRegion(atlas.findRegion("logo"));
	}
}
