package br.com.geppetto.heavybird.asset;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Cloud {
	public final RegionWrapper cloudWrapper = new RegionWrapper();
	
	Cloud() {}
	
	void init(TextureAtlas atlas) {
		cloudWrapper.setRegion(atlas.findRegion("cloud"));
	}
}
