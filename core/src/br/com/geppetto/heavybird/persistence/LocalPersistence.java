package br.com.geppetto.heavybird.persistence;

import br.com.geppetto.heavybird.controller.Mode;
import br.com.geppetto.heavybird.controller.ModeManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;

public class LocalPersistence {
	private static final int HISTORY = 10;
	private static final int DEFAULT_AVG = 8;
	
	private int bestScore;
	private int average;	
	private float adsIntervalTime;
	
	private int birdSkin;
	private int anchorSkin;
	
	public static final LocalPersistence instance = new LocalPersistence(); 
	
	private static final String PREFS_SOURCE = "heavyBird.prefs";
	
	private final Preferences prefs;
	
	private LocalPersistence() {
		prefs = Gdx.app.getPreferences(PREFS_SOURCE);
	}
	
	public void loadSkin() {
		birdSkin = prefs.getInteger("birdSkin", 0);
		anchorSkin = prefs.getInteger("anchorSkin", 0);
	}
	
	public void persistSkins(int birdSkin, int anchorSkin) {
		prefs.putInteger("birdSkin", birdSkin);
		prefs.putInteger("anchorSkin", anchorSkin);
		prefs.flush();
	}
	
	public void load() {
		Mode mode = ModeManager.INSTANCE.currentMode;
		bestScore = prefs.getInteger("bestScore" + mode.toString(), 0);
		float avrg = 0;
		for(int i = 0; i < HISTORY; i++) {
			int num = prefs.getInteger("score" + mode.toString() + i, DEFAULT_AVG);
			avrg += num;
		}
		avrg /= (float)HISTORY;
		average = MathUtils.ceil(avrg);
		adsIntervalTime = prefs.getFloat("adsIntervalTime", 0f);
	}
	
	public void persist(int score) {
		Mode mode = ModeManager.INSTANCE.currentMode;
		bestScore = Math.max(score, bestScore);
		prefs.putInteger("bestScore" + mode.toString(), bestScore);
		String scorePrefix = "score" + mode.toString() ;
		int last = prefs.getInteger(scorePrefix + "0", DEFAULT_AVG);
		for(int i = 1; i < HISTORY; i++) {
			int newValue = prefs.getInteger(scorePrefix + i, DEFAULT_AVG);
			prefs.putInteger(scorePrefix + i, last);
			last = newValue;
		}
		prefs.putInteger("score0", score);
		prefs.flush();
	}
	
	public void persistAdsIntervalTime(float adsIntervalTime) {
		this.adsIntervalTime = adsIntervalTime;
		prefs.putFloat("adsIntervalTime", adsIntervalTime);
		prefs.flush();
	}
	
	public int getBestScore() {
		return bestScore;
	}
	
	public int getAverage() {
		return average;
	}
	
	public float getAdsIntervalTime() {
		return adsIntervalTime;
	}
	
	public int getBirdSkin() {
		return birdSkin;
	}
	
	public int getAnchorSkin() {
		return anchorSkin;
	}
}
