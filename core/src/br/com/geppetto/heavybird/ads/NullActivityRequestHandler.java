package br.com.geppetto.heavybird.ads;

public class NullActivityRequestHandler implements IActivityRequestHandler{

	@Override
	public void showFooterAds(boolean show) {}

	@Override
	public void showFullScreenAdMenu() {}

	@Override
	public void showFullScreenAdOnGame() {}

}
