package br.com.geppetto.heavybird.ads;

public interface IActivityRequestHandler {
	void showFooterAds(boolean show);
	
	void showFullScreenAdMenu();
	
	void showFullScreenAdOnGame();
}
