package br.com.geppetto.heavybird.renderer;

import br.com.geppetto.heavybird.controller.controller.WorldController;
import br.com.geppetto.heavybird.screen.Initializable;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public class WorldRenderer implements Disposable, Initializable{
	private static final String TAG = WorldRenderer.class.getSimpleName();
	
	protected OrthographicCamera camera;
	private GUIRenderer guiRenderer;
	private SpriteBatch batch;
	protected WorldController worldController;
	
	public WorldRenderer(WorldController controller, OrthographicCamera camera) {
		worldController = controller;
		guiRenderer = new GUIRenderer();
		this.camera = camera;
		//cameraGUI = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		init();
	}
	
	@Override
	public void init() {
		Gdx.app.debug(TAG, "World Renderer has been initialized");
		batch = new SpriteBatch();
		guiRenderer.init(batch, worldController);
		
		camera.position.x = Constants.VIEWPORT_WIDTH / 2f;
		camera.position.y = Constants.VIEWPORT_HEIGHT / 2f;
		
		camera.update();
	}
	
	public void render() {
		worldController.cameraHelper.applyTo();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		worldController.render(batch);
		batch.end();
		guiRenderer.render();
	}
	
	public void resize(int width, int height) {
		camera.viewportHeight = (Constants.VIEWPORT_WIDTH / (float)width) * (float)height;
		camera.update();
		guiRenderer.resize(width, height);
	}

	@Override
	public void dispose() {
		guiRenderer.dispose();
	}
	
}
