package br.com.geppetto.heavybird.renderer;

import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public class GUIRenderer implements Disposable{
	private OrthographicCamera cameraGUI;
	private SpriteBatch batch;
	private GUIRenderable guiRenderable;
	
	public GUIRenderer() {
		cameraGUI = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
	}

	
	public void init(SpriteBatch batch, GUIRenderable guiRenderable) {
		this.batch = batch;
		this.guiRenderable = guiRenderable;
		cameraGUI.position.x = Constants.VIEWPORT_WIDTH / 2f;
		cameraGUI.position.y = Constants.VIEWPORT_HEIGHT / 2f;
		cameraGUI.update();
	}
	
	public void render() {
		batch.setProjectionMatrix(cameraGUI.combined);
		batch.begin();
		guiRenderable.renderGUI(batch);
		batch.end();
	}
	
	public void resize(int width, int height) {
		//cameraGUI.viewportHeight = (Constants.VIEWPORT_WIDTH / (float)width) * (float)height;
		//cameraGUI.update();
		guiRenderable.resize(width, height);
	}

	@Override
	public void dispose() {
		batch.dispose();
	}
}
