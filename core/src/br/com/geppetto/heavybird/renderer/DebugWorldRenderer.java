package br.com.geppetto.heavybird.renderer;

import br.com.geppetto.heavybird.controller.controller.WorldController;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class DebugWorldRenderer extends WorldRenderer {

	private ShapeRenderer shapeRenderer;
	
	public DebugWorldRenderer(WorldController controller,
			OrthographicCamera camera) {
		super(controller, camera);
		shapeRenderer = new ShapeRenderer();
	}

	@Override
	public void dispose() {
		super.dispose();
		shapeRenderer.dispose();
	}
	
	@Override
	public void render() {
		super.render();
		worldController.cameraHelper.applyTo();
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
		worldController.debugRender(shapeRenderer);
		shapeRenderer.end();
	}
}
