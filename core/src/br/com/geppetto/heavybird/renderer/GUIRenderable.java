package br.com.geppetto.heavybird.renderer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface GUIRenderable {
	void renderGUI(SpriteBatch batch);
	void resize(int width, int height);
}
