package br.com.geppetto.heavybird.controller;

public interface Actor {
	void actLeft();
	void actRight();
}
