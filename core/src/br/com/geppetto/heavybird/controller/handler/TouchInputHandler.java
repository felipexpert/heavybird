package br.com.geppetto.heavybird.controller.handler;

import br.com.geppetto.heavybird.controller.Actor;
import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class TouchInputHandler implements InputHandler {
	
	private static final float HALF = Constants.VIEWPORT_WIDTH / 2f;
	
	protected final Actor actor;
	
	protected final Vector2 worldDevidedRealSize = new Vector2();
	
	private boolean pressed;
	
	private boolean menuItemPressed;

	public TouchInputHandler() {
		actor = null;
	}
	
	public TouchInputHandler(Actor actor) {
		this.actor = actor;
	}
	
	@Override
	public void handleInput() {
		if(Gdx.input.isTouched()) {
			if(!pressed) {
				if(Gdx.input.getX() * worldDevidedRealSize.x < HALF) {
					actor.actLeft();
				} else {
					actor.actRight();
				}
				pressed = true;
			}
		} else if(pressed) {
			pressed = false;
		}
	}

	@Override
	public GenericMenuItem isMenuItemPressed(Array<? extends GenericMenuItem> menuItens) {
		if(Gdx.input.isTouched()) {
			menuItemPressed = true;
		} else if(menuItemPressed) {
			float x = Gdx.input.getX() * worldDevidedRealSize.x;
			float y = Constants.VIEWPORT_HEIGHT - (Gdx.input.getY() * worldDevidedRealSize.y);
			menuItemPressed = false;
			for(GenericMenuItem m : menuItens) {
				if(m.wasClicked(x, y)) {
					return m;
				}
			}
		}
		return null;
	}
	
	@Override
	public boolean isMenuItemPressed(GenericMenuItem menuItem) {
		if(Gdx.input.isTouched()) {
			menuItemPressed = true;
		} else if(menuItemPressed) {
			float x = Gdx.input.getX() * worldDevidedRealSize.x;
			float y = Constants.VIEWPORT_HEIGHT - (Gdx.input.getY() * worldDevidedRealSize.y);
			menuItemPressed = false;
			return menuItem.wasClicked(x, y);
		}
		return false;
	}

	@Override
	public void setScreenResolution(float x, float y) {
		worldDevidedRealSize.x = Constants.VIEWPORT_WIDTH / x;
		worldDevidedRealSize.y = Constants.VIEWPORT_HEIGHT / y;
	}
}
