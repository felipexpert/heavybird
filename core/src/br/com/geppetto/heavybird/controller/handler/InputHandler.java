package br.com.geppetto.heavybird.controller.handler;

import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;

import com.badlogic.gdx.utils.Array;

public interface InputHandler {
	void handleInput();
	boolean isMenuItemPressed(GenericMenuItem menuItem);
	GenericMenuItem isMenuItemPressed(Array<? extends GenericMenuItem> menuItem);
	void setScreenResolution(float x, float y);
}
