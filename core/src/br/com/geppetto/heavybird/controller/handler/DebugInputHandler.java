package br.com.geppetto.heavybird.controller.handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import br.com.geppetto.heavybird.controller.Actor;

public class DebugInputHandler extends TouchInputHandler {

	private boolean leftPressed;
	private boolean rightPressed;
	
	public DebugInputHandler(Actor actor) {
		super(actor);
	}
	
	@Override
	public void handleInput() {
		super.handleInput();
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			leftPressed = true;
		} else {
			if(leftPressed) {
				actor.actLeft();
				leftPressed = false;
			}
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			rightPressed = true;
		} else {
			if(rightPressed) {
				actor.actRight();
				rightPressed = false;
			}
		}
	}

}
