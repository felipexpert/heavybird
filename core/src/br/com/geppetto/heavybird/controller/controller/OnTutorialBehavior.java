package br.com.geppetto.heavybird.controller.controller;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class OnTutorialBehavior implements ControllerBehavior{
	private final WorldController controller;
	
	private final AbstractGameObject handRight;
	private final AbstractGameObject handLeft;
	
	private AbstractGameObject currHand; 
	
	private final AbstractGameObject getReady;
	
	private static final int MAX_POINTING_TIMES = 6;
	private int leftPointingTimes;
	private int rightPointingTimes;
	
	private boolean rightArrowPressed;
	private float handPressedTime;
	
	private float time;
	
	private final float[] y = new float[3]; 
	private int currPosition;
	
	public OnTutorialBehavior(WorldController controller) {
		this.controller = controller;
		handRight = GameObjectFactory.instance.getGameObject(GameObject.HAND_RIGHT);
		handLeft = GameObjectFactory.instance.getGameObject(GameObject.HAND_LEFT);
		
		currHand = handRight;
		
		getReady = GameObjectFactory.instance.getGameObject(GameObject.GET_READY);
		
		getReady.updateCenterX(Constants.VIEWPORT_WIDTH / 2f);
		getReady.updateCenterY(Constants.VIEWPORT_HEIGHT / 4f * 3f - 1f);
				
		y[0] = Constants.VIEWPORT_HEIGHT - 3f;
		y[1] = Constants.VIEWPORT_HEIGHT / 2f - 1f;
		y[2] = GameObjectFactory.instance.ANCHOR_SIZE.y / 2f -
				GameObjectFactory.instance.HAND_LEFT_SIZE.y / 4f;
		
		handRight.updatePosition(Constants.VIEWPORT_WIDTH * .75f - 1f, y[0]);
		handLeft.updatePosition(Constants.VIEWPORT_WIDTH * .25f - 1f, y[0]);
	}

	@Override
	public void render(SpriteBatch batch) {
		controller.clouds.render(batch);
		controller.pipes.render(batch);
		controller.walls.render(batch);
		if((int)(time * 4f) % 2 == 0)
			batch.setColor(0f, .5f, 1f, .5f);
		controller.bird.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		controller.digitsController.render(batch);
		controller.symbolicControll.render(batch);
		currHand.render(batch);
		getReady.render(batch);
		controller.mainSoundButton.render(batch);
	}

	@Override
	public void update(float delta) {
		if(controller.handler.isMenuItemPressed(controller.mainSoundButton)) {
			controller.mainSoundButton.toggle();
		}
		time += delta;
		handPressedTime += delta;
		if(rightPointingTimes + leftPointingTimes < MAX_POINTING_TIMES) {
			if(rightPointingTimes == leftPointingTimes) {
				handRight.updateAnimation(delta);
				if(rightArrowPressed && handPressedTime >= .025f) {
					controller.symbolicControll.pressLeft();
					rightArrowPressed = false;
					handPressedTime = 0f;
				}
				if(handRight.isAnimationFinished()) {
					rightPointingTimes ++;
					handRight.resetAnimation();
					currHand = handLeft;
				}
			} else {
				handLeft.updateAnimation(delta);
				if(!rightArrowPressed && handPressedTime >= .025f) {
					controller.symbolicControll.pressRight();
					rightArrowPressed = true;
					handPressedTime = 0f;
				}
				if(handLeft.isAnimationFinished()) {
					currPosition = Math.min(currPosition + 1, y.length - 1);
					handRight.updatePositionY(y[currPosition]);
					handLeft.updatePositionY(y[currPosition]);
					leftPointingTimes ++;
					handLeft.resetAnimation();
					currHand = handRight;
				}
			}
		} else { // Tutor is done
			rightPointingTimes = 0;
			leftPointingTimes = 0;
			time = 0f;
			controller.onGameBehavior();
		}
		controller.symbolicControll.update(delta);
	}
	
	
}
