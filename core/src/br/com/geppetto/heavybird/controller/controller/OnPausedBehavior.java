package br.com.geppetto.heavybird.controller.controller;

import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.gameObject.menuitem.MenuItem;
import br.com.geppetto.heavybird.gameObject.menuitem.SoundButton;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class OnPausedBehavior implements ControllerBehavior {

	private final WorldController controller;
	
	private final GenericMenuItem resume;
	private final SoundButton sound;
	private final GenericMenuItem exit;
	private final Array<GenericMenuItem> itens = new Array<GenericMenuItem>();
	
	public OnPausedBehavior(WorldController controller) {
		this.controller = controller;
		
		resume = new GenericMenuItem(MenuItem.RESUME);
		sound = new SoundButton();
		exit = new GenericMenuItem(MenuItem.EXIT);
		
		float centerX = Constants.VIEWPORT_WIDTH / 2f;
		float centerY = Constants.VIEWPORT_HEIGHT / 2f;
		
		sound.setCenterX(centerX);
		sound.setCenterY(centerY);
		
		resume.setCenterX(centerX - (resume.getWidth() / 2f + sound.getWidth() / 2f)
				- .5f);
		resume.setCenterY(centerY);
		
		exit.setCenterX(centerX + (resume.getWidth() / 2f + sound.getWidth() / 2f)
				+ .5f);
		exit.setCenterY(centerY);
		
		itens.add(resume);
		itens.add(sound);
		itens.add(exit);
	}
	
	@Override
	public void render(SpriteBatch batch) {
		controller.clouds.render(batch);
		controller.pipes.render(batch);
		controller.walls.render(batch);
		controller.bird.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		resume.render(batch);
		sound.render(batch);
		exit.render(batch);
	}

	@Override
	public void update(float delta) {
		GenericMenuItem item = controller.handler.isMenuItemPressed(itens);
		controller.exitCommand.update(delta);
		if(item == resume) {
			controller.onGameBehavior();
			controller.abortBackToMenu();
		} else if(item == sound) {
			sound.toggle();
		} else if(item == exit) {
			controller.backToMenu();
		}
	}

}
