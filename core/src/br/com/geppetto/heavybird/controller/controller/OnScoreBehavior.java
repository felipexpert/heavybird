package br.com.geppetto.heavybird.controller.controller;

import br.com.geppetto.heavybird.ads.External;
import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.controller.ModeManager;
import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.gameObject.menuitem.MenuItem;
import br.com.geppetto.heavybird.persistence.LocalPersistence;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.Digit.Digits;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Array;

public class OnScoreBehavior implements ControllerBehavior{

	private static final float padding = .0625f;
	private static final float width = .75f;
	private static final float height = 1f;
	
	private final WorldController controller;
	
	private final Array<GenericMenuItem> buttons = new Array<GenericMenuItem>();
	
	private final GenericMenuItem ok;
	private final GenericMenuItem leaderboard;
	private final GenericMenuItem share;
	private final GenericMenuItem scoreBackground;
	private final GenericMenuItem score;
	private final Digits scoreDigit;
	private final GenericMenuItem best;
	private final Digits bestDigit;
	
	private final AbstractGameObject gameOver;
	private final GenericMenuItem rateButton;
	private final GenericMenuItem exit;
	
	private float progress;
	private static final float READY = 1.125f; 
	
	private final float stopAt = 
			(Constants.VIEWPORT_HEIGHT - GameObjectFactory.instance.SCORE_BACKGROUND_SIZE.y) / 2f - 1f;
	
	private AbstractGameObject medal;
	
	private boolean rate;
	
	public OnScoreBehavior(WorldController controller) {
		this.controller = controller;
		
		float center = Constants.VIEWPORT_WIDTH / 2f; 
		
		ok = new GenericMenuItem(MenuItem.OK);
		ok.setCenterX(center);
		buttons.add(ok);
		buttons.add(controller.mainSoundButton);
		
		scoreBackground = new GenericMenuItem(MenuItem.SCORE_BACKGROUND);
		scoreBackground.setCenterX(center);
		
		score = new GenericMenuItem(MenuItem.SCORE);
		score.setCenterX(center);
		scoreDigit = new Digits(Assets.instance.digits, 
				padding, width, height);
		
		best = new GenericMenuItem(MenuItem.BEST);
		best.setCenterX(center);;
		bestDigit = new Digits(Assets.instance.digits, 
				padding, width, height);
		
		leaderboard = new GenericMenuItem(MenuItem.LEADERBOARD);
		leaderboard.setX(scoreBackground.getCenterX() - leaderboard.getWidth() - .25f);
		buttons.add(leaderboard);
		
		share = new GenericMenuItem(MenuItem.SHARE);
		share.setX(scoreBackground.getCenterX() 
				+ .25f);
		buttons.add(share);		
		
		gameOver = GameObjectFactory.instance.getGameObject(GameObject.GAME_OVER);
		gameOver.updateCenterX(center);
		gameOver.updatePositionY(Constants.VIEWPORT_HEIGHT - gameOver.getHeight() - 4f);
		
		rateButton = new GenericMenuItem(MenuItem.RATE);
		rateButton.setCenterX(center);
		rateButton.setY(stopAt - rateButton.getHeight() - .5f);
		
		buttons.add(rateButton);
		
		exit = new GenericMenuItem(MenuItem.EXIT);
		exit.setX(Constants.VIEWPORT_WIDTH - exit.getWidth() - 
				GameObjectFactory.instance.WALL_SIZE.x);
		exit.setY(Constants.VIEWPORT_HEIGHT - exit.getHeight());
		
		buttons.add(exit);
		
		updateScoreSquare(Constants.VIEWPORT_HEIGHT);
		GameObjectFactory.instance.MEDAL_POSITION.x = score.getX() - GameObjectFactory.instance.MEDAL_SIZE.x - .5f;
	}
	
	private void updateScoreSquare(float y) {
		scoreBackground.setY(y);
		ok.setY(y + scoreBackground.getHeight() - ok.getHeight() - .25f);
		score.setY(ok.getY() - score.getHeight() - 1f);
		scoreDigit.updateByLeftCenterCorner(controller.getPoints(),
				score.getX() + score.getWidth() + .5f, 
				score.getCenterY());
		best.setY(score.getY() - best.getHeight() - 1f);
		bestDigit.updateByLeftCenterCorner(LocalPersistence.instance.getBestScore(),
				best.getX() + best.getWidth() + .75f,
				best.getCenterY());
		float leaderboardY = y + .5f;
		leaderboard.setY(leaderboardY);
		share.setY(leaderboardY);
		GameObjectFactory.instance.MEDAL_POSITION.y = scoreBackground.getY() + scoreBackground.getHeight() / 2f - GameObjectFactory.instance.MEDAL_SIZE.y / 2f;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		controller.clouds.render(batch);
		controller.pipes.render(batch);
		controller.walls.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		if(rate) {
			rateButton.render(batch);
		}
		gameOver.render(batch);
		batch.setColor(1f, 1f, 1f, .75f);
		scoreBackground.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
		ok.render(batch);
		score.render(batch);
		scoreDigit.render(batch);
		best.render(batch);
		bestDigit.render(batch);
		leaderboard.render(batch);
		share.render(batch);
		controller.mainSoundButton.render(batch);
		exit.render(batch);
		if(medal != null) medal.render(batch);
	}

	@Override
	public void update(float delta) {
		GenericMenuItem button = controller.handler.isMenuItemPressed(buttons);
		controller.exitCommand.update(delta);
		if(button == ok) {
			rate = false;
			progress = 0f;
			updateScoreSquare(Constants.VIEWPORT_HEIGHT);
			controller.init();
			controller.abortBackToMenu();
		} else if(button == controller.mainSoundButton) {
			SoundManager.instance.toggle();
		} else if(button == leaderboard) {
			External.instance.iGoogleServices.submitScore(ModeManager.INSTANCE.currentMode, LocalPersistence.instance.getBestScore());
			External.instance.iGoogleServices.showScores(ModeManager.INSTANCE.currentMode);
			if(!External.instance.iGoogleServices.isSignedIn()) {
				External.instance.iGoogleServices.signIn();
			}
		} else if(button == share) {
			
		} else if(button == rateButton && rate) {
			External.instance.iGoogleServices.rateGame();
		} else if(button == exit) {
			controller.backToMenu();
		}
		
		progress = Math.min(progress + delta / READY, 1f);
		float position = Interpolation.elastic.apply(Constants.VIEWPORT_HEIGHT,
				stopAt, progress);
		
		if(progress != 1f) {
			updateScoreSquare(position);
		}
	}
	
	public void prepare(int prevBestScore) {
		float points = controller.getPoints();
		float average = LocalPersistence.instance.getAverage();
		if(points > prevBestScore) {
			if(points >= 15f) rate = true;
			medal = GameObjectFactory.instance.getGameObject(GameObject.PLATINUM);
		} else if(points >= average * 2.75f) {
			medal = GameObjectFactory.instance.getGameObject(GameObject.GOLD);
		} else if(points >= average * 1.75f) {
			medal = GameObjectFactory.instance.getGameObject(GameObject.SILVER);
		} else if(points >= average * 1.25f) {
			medal = GameObjectFactory.instance.getGameObject(GameObject.BRONZE);
		} else {
			medal = null;
		}
	}

}
