package br.com.geppetto.heavybird.controller.controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface ControllerBehavior {
	void render(SpriteBatch batch);
	void renderGUI(SpriteBatch batch);
	void update(float delta);
}
