package br.com.geppetto.heavybird.controller.controller;

import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class OnDeathBehavior implements ControllerBehavior {
	
	private final WorldController controller;
	
	private float time;
	private boolean playedFalling;
	
	public OnDeathBehavior(WorldController controller) {
		this.controller = controller;
	}
	
	@Override
	public void render(SpriteBatch batch) {
		controller.clouds.render(batch);
		controller.pipes.render(batch);
		controller.walls.render(batch);
		controller.bird.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		controller.digitsController.render(batch);
		controller.mainSoundButton.render(batch);
	}

	@Override
	public void update(float delta) {
		if(controller.handler.isMenuItemPressed(controller.mainSoundButton)) {
			SoundManager.instance.toggle();
		}
		time += delta;
		controller.pipes.update(delta);
		controller.bird.update(delta);
		if(time >= 1.3f) {
			time = 0f;
			playedFalling = false;
			controller.onScoreBehavior();
		} else if(time >= .8f) {
			if(!playedFalling) {
				SoundManager.instance.play(Sound.FALLING);
				playedFalling = true;
			}
		}
		controller.symbolicControll.update(delta);
	}

}
