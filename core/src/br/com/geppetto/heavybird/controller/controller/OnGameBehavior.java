package br.com.geppetto.heavybird.controller.controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class OnGameBehavior implements ControllerBehavior{

	private final WorldController controller;
	
	public OnGameBehavior(WorldController controller) {
		this.controller = controller;
	}	
	
	@Override
	public void render(SpriteBatch batch) {
		controller.clouds.render(batch);
		controller.pipes.render(batch);
		controller.walls.render(batch);
		controller.bird.render(batch);
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		controller.digitsController.render(batch);
		controller.symbolicControll.render(batch);
	}

	@Override
	public void update(float delta) {
		controller.handler.handleInput();
		controller.cameraHelper.update(delta);
		controller.walls.update();
		controller.walls.testBirdWallsColision(controller.bird, controller);
		controller.clouds.update(delta);
		controller.pipes.update(delta);
		controller.bird.update(delta);
		controller.symbolicControll.update(delta);
	}

}
