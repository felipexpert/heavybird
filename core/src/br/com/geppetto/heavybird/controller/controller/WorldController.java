package br.com.geppetto.heavybird.controller.controller;

import br.com.geppetto.heavybird.ads.External;
import br.com.geppetto.heavybird.controller.Actor;
import br.com.geppetto.heavybird.controller.DigitsController;
import br.com.geppetto.heavybird.controller.AdsIntervalHelper;
import br.com.geppetto.heavybird.controller.ModeManager;
import br.com.geppetto.heavybird.controller.handler.DebugInputHandler;
import br.com.geppetto.heavybird.controller.handler.InputHandler;
import br.com.geppetto.heavybird.controller.handler.TouchInputHandler;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.bird.Bird;
import br.com.geppetto.heavybird.gameObject.cloud.Clouds;
import br.com.geppetto.heavybird.gameObject.menuitem.SoundButton;
import br.com.geppetto.heavybird.gameObject.pipe.Pipes;
import br.com.geppetto.heavybird.gameObject.symboliccontrolls.SymbolicControll;
import br.com.geppetto.heavybird.gameObject.wall.Walls;
import br.com.geppetto.heavybird.persistence.LocalPersistence;
import br.com.geppetto.heavybird.renderer.GUIRenderable;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.CameraHelper;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.command.ExitToMenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class WorldController extends InputAdapter implements Actor, GUIRenderable{
	
	private static final String TAG = WorldController.class.getSimpleName();
	
	public static Color clearGameColor = new Color(0x64 / 255.0f, 0x95 / 255.0f, 0xed / 255.0f, 1f);	
	public final CameraHelper cameraHelper;
	final Walls walls;
	final Pipes pipes;
	final Bird bird;
	final Clouds clouds;
	final DigitsController digitsController = new DigitsController();
	final InputHandler handler;
	final SymbolicControll symbolicControll = new SymbolicControll();
	final SoundButton mainSoundButton = new SoundButton(); // must be initialized before OnScoreBehavior
	final ExitToMenu exitCommand = new ExitToMenu();
	//final AbstractGameObject rightArrow = GameObjectFactory.instance.getGameObject(GameObject.RIGHT_ARROW);
	//final AbstractGameObject leftArrow = GameObjectFactory.instance.getGameObject(GameObject.LEFT_ARROW);
	
	private ControllerBehavior currControllerBehavior;
	
	private final OnGameBehavior onGameBehavior = new OnGameBehavior(this);
	private final OnDeathBehavior onDeathBehavior = new OnDeathBehavior(this);
	private final OnScoreBehavior onScoreBehavior = new OnScoreBehavior(this);
	private final OnPausedBehavior onPausedBehavior = new OnPausedBehavior(this);
	
	private int prevBestScore;
	
	private final AdsIntervalHelper adsIntervalHelper = new AdsIntervalHelper();
	
	public WorldController(OrthographicCamera camera) {
		
		//bird.setDrawableHelper(new NonAnimatedDrawableHelper(Assets.instance.bird.bird));
		//bird.updatePosition(4f, 0f);
		bird = new Bird();
		cameraHelper = new CameraHelper(camera, bird.getBirdObject());
		walls = new Walls(cameraHelper);
		clouds = new Clouds(cameraHelper);
		mainSoundButton.setX(0f + GameObjectFactory.instance.WALL_SIZE.x);
		mainSoundButton.setY(Constants.VIEWPORT_HEIGHT
				- mainSoundButton.getHeight());
		
		pipes = new Pipes(this, bird, ModeManager.INSTANCE.currentMode.pipes, cameraHelper);
		handler = 
				Gdx.app.getType() != ApplicationType.Desktop ?
				new TouchInputHandler(this) :
				new DebugInputHandler(this);
				
		//leftArrow.updatePosition(0f, 0f);
		//rightArrow.updatePosition(Constants.VIEWPORT_WIDTH / 2f, 0f);
		LocalPersistence.instance.load();
		init();
		currControllerBehavior = new OnTutorialBehavior(this);
	}
	
	public void init() {
		onGameBehavior();
		bird.init();
		cameraHelper.reset();
		pipes.init();
		initWalls();
		clouds.init();
		digitsController.init();
		Gdx.app.debug(TAG, "World Controller has been initialized");
	}
	
	private void initWalls() {
		walls.init(bird.getBirdObject().getCenter().y - Constants.VIEWPORT_HEIGHT);
	}
	
	public void render(SpriteBatch batch) {
		currControllerBehavior.render(batch);
	}
	
	public void renderGUI(SpriteBatch batch) {
		currControllerBehavior.renderGUI(batch);
	}
	
	public void informPoints(int points) {
		digitsController.informPoints(points);
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		pipes.debugRender(shapeRenderer);
		bird.debugRender(shapeRenderer);
	}
	
	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.ESCAPE:
		case Keys.BACK:
			if(currControllerBehavior == onGameBehavior)
				onPauseBehavior();
			else if(currControllerBehavior == onPausedBehavior)
				onGameBehavior();
			break;
		}
		return false;
	}
	
	@Override
	public void actLeft() {
		symbolicControll.pressLeft();
		bird.actLeft();
	}
	
	@Override
	public void actRight() {
		symbolicControll.pressRight();
		bird.actRight();
	}
	
	public void update(float delta) {
		adsIntervalHelper.update(delta);
		currControllerBehavior.update(delta);
	}
	
	@Override
	public void resize(int x, int y) {
		handler.setScreenResolution(x, y);
		initWalls();
	}
	
	public float getBirdX() {
		return bird.getBirdObject().getX();
	}
	
	public float getBirdY() {
		return bird.getBirdObject().getY();
	}
	
	public float getBirdWidth() {
		return bird.getBirdObject().getWidth();
	}
	
	public float getBirdHeight() {
		return bird.getBirdObject().getHeight();
	}
	
	public void onDeath() {
		Gdx.app.debug(TAG, "You died");
		prevBestScore = LocalPersistence.instance.getBestScore();
		LocalPersistence.instance.persist(digitsController.getPoints());
		LocalPersistence.instance.load();
		adsIntervalHelper.manage();
		Gdx.app.debug(TAG, "Best score: " + LocalPersistence.instance.getBestScore());
		Gdx.app.debug(TAG, "Average: " + LocalPersistence.instance.getAverage());
		//cameraHelper.noMovement();
		SoundManager.instance.play(Sound.HIT);
		onDeathBehavior();
	}
	
	void backToMenu() {
		exitCommand.execute(adsIntervalHelper);
	}
	
	void abortBackToMenu() {
		exitCommand.stop();
	}
	
	int getPoints() {
		return digitsController.getPoints();
	}
	
	void onGameBehavior() {
		External.instance.handler.showFooterAds(false);
		currControllerBehavior = onGameBehavior;
	}
	
	void onPauseBehavior() {
		External.instance.handler.showFooterAds(true);
		currControllerBehavior = onPausedBehavior;
	}
	
	void onDeathBehavior() {
		External.instance.handler.showFooterAds(false);
		currControllerBehavior = onDeathBehavior;
	}
	
	void onScoreBehavior() {
		External.instance.handler.showFooterAds(true);
		if(adsIntervalHelper.showAds()) {
			Gdx.app.debug(TAG, "Interstitial ADS");
			External.instance.handler.showFullScreenAdOnGame();
		}
		onScoreBehavior.prepare(prevBestScore);
		currControllerBehavior = onScoreBehavior;
	}
}
