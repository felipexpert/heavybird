package br.com.geppetto.heavybird.controller;

import br.com.geppetto.heavybird.gameObject.pipe.PipeType;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.utils.Array;

public enum Mode {
	EASY(-13f, -6.25f, Constants.EASY), HARDCORE(-17f, -8f, Constants.HARDCORE);
	public final float g;
	public final float minVelocity;
	public final Array<PipeType> pipes;
	
	
	
	private Mode(float G, float min, Array<PipeType> pipes) {
		this.g = G;
		minVelocity = min;
		this.pipes = pipes;
	}
}
