package br.com.geppetto.heavybird.controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.Digit.Digits;

public class DigitsController {
	private static final float WIDTH = 1.8f;
	private static final float HEIGHT = 3f;
	private static final float X = Constants.VIEWPORT_WIDTH / 2f;
	private static final float Y = Constants.VIEWPORT_HEIGHT - HEIGHT / 2 - 1.5f;
	private static final float PADDING = .125f;
	
	private final Digits digits;
	
	private int points;
	
	public void init() {
		informPoints(0);
	}
	
	public DigitsController() {
		digits = new Digits(Assets.instance.digits,
				PADDING, WIDTH, HEIGHT);
	}
	
	public void informPoints(int points) {
		this.points = points;
		digits.updateByCenter(points, X, Y);
	}
	
	public int getPoints() {
		return points;
	}
	
	public void render(SpriteBatch batch) {
		digits.render(batch);
	}
}
