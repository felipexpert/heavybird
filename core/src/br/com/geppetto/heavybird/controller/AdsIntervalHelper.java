package br.com.geppetto.heavybird.controller;

import br.com.geppetto.heavybird.persistence.LocalPersistence;

public class AdsIntervalHelper {
	private static final float ADS = 420f; // 7 minutes
	//private static final float ADS = 900f; // 15 minutes
	private float time;	
	private boolean showAds;

	public void update(float delta) {
		time += delta;
	}
	
	public void increaseTime() {
		float lTime = LocalPersistence.instance.getAdsIntervalTime();
		lTime += time;
		time = 0f;
		LocalPersistence.instance.persistAdsIntervalTime(lTime);
	}
	
	public void manage() { // I can save the lTime together with the best score to use only one flush()
		float lTime = LocalPersistence.instance.getAdsIntervalTime();
		lTime += time;
		time = 0f; // reseting time
		if(lTime >= ADS) {
			showAds = true;
			lTime -= ADS;
		}

		LocalPersistence.instance.persistAdsIntervalTime(lTime);
	}
	
	public boolean showAds() {
		boolean s = showAds;
		showAds = false;
		return s;
	}
}
