package br.com.geppetto.heavybird.util;

import br.com.geppetto.heavybird.gameObject.pipe.PipeType;

import com.badlogic.gdx.utils.Array;

public class Constants {
	private Constants() {}
	
	public static final float VIEWPORT_WIDTH = 15f;
	public static final float VIEWPORT_HEIGHT = 20f;
	
	public static final String TEXTURE_ATLAS_OBJECTS = "images/heavyBird.pack";
	
	public static final int BIRD_SKINS = 6;
	public static final int ANCHOR_SKINS = 5;
	
	public static final Array<PipeType> EASY;
	public static final Array<PipeType> HARDCORE;
	
	static {
		
		float normalPipesH = 3.75f;
		float velocityH = 5f;
		float normalPipes2H = 4.25f;
		float normalPipes3H = 5.5f;
		float movingPipesH = 6f;
		float decrementH = .125f;
		
		float normalPipesE = 5.25f;
		float velocityE = 3f;
		//float normalPipes2E = 10f;
		float normalPipes3E = 6.5f;
		float movingPipesE = 7f;
		float decrementE = .25f;
		
		EASY = new Array<PipeType>();
		HARDCORE = new Array<PipeType>();
		
		for(int j = 0; j < 3; j++) {
			for(int i = 0; i < 3; i++) {
				HARDCORE.add(new PipeType(normalPipesH, 20f, 5));//5
				HARDCORE.add(new PipeType(normalPipesH, 20f, velocityH, 12f, 2));//2
				HARDCORE.add(new PipeType(movingPipesH, 20f, 4f, 1));
				
				HARDCORE.add(new PipeType(normalPipesH, 20f, 2));//5
				HARDCORE.add(new PipeType(normalPipesH, 20f, velocityH, 12f, 1));//2
				HARDCORE.add(new PipeType(movingPipesH, 20f, 4f, 4f, 12f, 1));
				
				HARDCORE.add(new PipeType(normalPipes3H, 15, 3));
				HARDCORE.add(new PipeType(normalPipesH, 20, 3));
				normalPipesH -= decrementH;
				//normalPipes2H -= decrementH;
				normalPipes3H -= decrementH * 2f;
			}
			
			HARDCORE.add(new PipeType(normalPipesH, 10f, 8));//5
			HARDCORE.add(new PipeType(normalPipes2H, 20f, 3));//5
			HARDCORE.add(new PipeType(normalPipes2H, 4f, 12f, 2));//2
			HARDCORE.add(new PipeType(movingPipesH, 20f, 4f, 2));
			HARDCORE.add(new PipeType(movingPipesH, 20f, 4f, 4f, 12f, 2));
		}
		
		for(int j = 0; j < 5; j++) {
			for(int i = 0; i < 3; i++) {
				EASY.add(new PipeType(normalPipesE, 10f, 5));//5
				EASY.add(new PipeType(normalPipesE, 10f, velocityE, 12f, 2));//2
				EASY.add(new PipeType(movingPipesE, 10f, .25f, 1));
				
				EASY.add(new PipeType(normalPipesE, 10f, 2));//5
				EASY.add(new PipeType(normalPipesE, 10f, velocityE, 12f, 1));//2
				EASY.add(new PipeType(movingPipesE, 10f, .25f, 1));
				
				EASY.add(new PipeType(normalPipes3E, 7, 3));
				EASY.add(new PipeType(normalPipesE, 10, 3));
				normalPipesE -= decrementE;
				//normalPipes2E -= decrementE;
				normalPipes3E -= decrementE * 2f;
			}
			
			EASY.add(new PipeType(normalPipesE, 6f, 8));//5
			EASY.add(new PipeType(normalPipesE, 6f, 6));//5
		//new PipeType(passageDistance, distance, closeVel, nextLevel)
			EASY.add(new PipeType(normalPipesE, 6f, .25f, 4));//2
			//EASY.add(new PipeType(movingPipesE, 10f, 4f, 4f, 12f, 2));
		}
	}
}
