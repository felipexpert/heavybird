package br.com.geppetto.heavybird.util.Digit;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Digits {
	private final DigitKeeper digitKeeper;
	private final Array<Digit> digits;
	private float digitsPadding;
	private final Vector2 dimension;
	
	private int number;
	
	public Digits(DigitKeeper digitKeeper, float digitsPadding,
			float width, float height) {
		this.digitKeeper = digitKeeper;
		this.digits = new Array<Digit>();
		Digit dg = new Digit();
		digits.add(dg);
		this.digitsPadding = digitsPadding;
		dimension = new Vector2(width, height);
	}
	
	public void setDimention(float width, float height) {
		dimension.x = width;
		dimension.y = height;
	}
	
	private static int pow(int number, int n) {
		if(n == 0)
			return 1;
		if(n == 1)
			return number;
		 return number * pow(number, n - 1); 
	}
	
	public void updateByLeftDownCorner(int number, float x, float y) {
		arrangeDigits(number);
		for(int i = 0; i < digits.size; i++) {
			digits.get(i).position.x = x + (i * (dimension.x + digitsPadding));
			digits.get(i).position.y = y;
		}
	}
	
	public void updateByLeftCenterCorner(int number, float x, float centerY) {
		arrangeDigits(number);
		for(int i = 0; i < digits.size; i++) {
			digits.get(i).position.x = x + (i * (dimension.x + digitsPadding));
			digits.get(i).position.y = centerY;
		}
	}
	
	public void updateByRightDownCorner(int number, float x, float y) {
		arrangeDigits(number);
		int df = digits.size - 1;
		for(int i = 0; i < digits.size; i++) {
			float difference = df - i;
			digits.get(i).position.x = 
					(x - dimension.x) - (difference * (dimension.x + digitsPadding));
			
			digits.get(i).position.y = y;
		}
	}
	
	public void updateByCenter(int number, float x, float y) {
		arrangeDigits(number);
		
		x -= dimension.x / 2f + ((digitsPadding + dimension.x) * ((digits.size - 1) / 2f));
		
		for(int i = 0; i < digits.size; i++) {
			digits.get(i).position.x = x + (i * (dimension.x + digitsPadding));
			digits.get(i).position.y = y;
		}
	}
	
	private void arrangeDigits(int number) {
		this.number = number;
		{
			//int d = -1;
			int d = (number == 0 ? 0 : -1);
			while(true) {
				d++;
				if(number < pow(10, d)) {
					break;
				}
			}
			if(digits.size < d) {
				int difference = d - digits.size;
				while(difference > 0) {
					Digit dig = new Digit();
					digits.add(dig);
					difference--;
				}
			} else if(digits.size > d) {
				int difference = digits.size - d;
				while(difference > 0) {
					digits.removeIndex(digits.size - 1);
					difference--;
				}
			}
		}
		
		// If it is here "d" and "digits.size" are equal
		// Wonderful!!
		int num = digits.size - 1;
		for(int i = num; i >= 0; i--) {
				int pow = pow(10, i);
				int d2 = number / pow; 
				digits.get(num - i).imgDigit = 
						digitKeeper.getImgDigit(d2);
				number -= d2 * pow;
		}
	}
	
	public float getDigitsPadding() {
		return digitsPadding;
	}
	
	public void setDigitsPadding(float padding) {
		digitsPadding = padding;
	}
	
	public int getNumber() {
		return number;
	}
	
	public void render(SpriteBatch batch) {
		for(Digit d : digits) {
			batch.draw(d.imgDigit.getRegion().getTexture(),
					d.position.x, d.position.y, 
					0f, 0f, 
					dimension.x, dimension.y, 
					1f,
					1f,  
					0f, 
					d.imgDigit.getRegion().getRegionX(),
					d.imgDigit.getRegion().getRegionY(), 
					d.imgDigit.getRegion().getRegionWidth(),
					d.imgDigit.getRegion().getRegionHeight(), 
					false, false);
		}
		
	}
}
