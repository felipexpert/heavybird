package br.com.geppetto.heavybird.util.Digit;

import br.com.geppetto.heavybird.asset.RegionWrapper;


public interface DigitKeeper {
	RegionWrapper getImgDigit(int digit);
}
