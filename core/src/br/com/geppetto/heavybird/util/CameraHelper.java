package br.com.geppetto.heavybird.util;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class CameraHelper {
	
	private final float FOLLOW_SPEED = 4f;
	
	private OrthographicCamera camera;
	
	private final Vector2 position;
	private final AbstractGameObject target;
	
	private static final float BIRD_VISION_OFFSET = 5.75f;
	
	private static final float VIEW_OFFSET = 2.5f;
	
	private final UpdateBehavior normalUpdateBehavior = new NormalUpdateBehavior();
	private final UpdateBehavior staticUpdateBehavior = new StaticUpdateBehavior();
	private UpdateBehavior currentUpdateBehavior = normalUpdateBehavior;
	
	public CameraHelper(OrthographicCamera camera, AbstractGameObject target) {
		this.camera = camera;
		position = new Vector2(Constants.VIEWPORT_WIDTH / 2f,
							   Constants.VIEWPORT_HEIGHT / 2f);
		
		this.target = target;
	}
	
	public void reset() {
		position.y = 0f;
	}
	
	public void update(float delta) {
		currentUpdateBehavior.update(delta);
	}
	
	public void applyTo() {
		camera.position.x = position.x;
		camera.position.y = position.y;
		camera.update();
	}
	
	public boolean isUnderVision(AbstractGameObject object) {
		final float halfWidth = camera.viewportWidth / 2f + VIEW_OFFSET;
		final float halfHeight = camera.viewportHeight / 2f + VIEW_OFFSET;
		return object.getImgX() <= camera.position.x + halfWidth &&
			   object.getImgX() + object.getImgWidth() >= camera.position.x - halfWidth &&
			   object.getImgY() <= camera.position.y + halfHeight &&
			   object.getImgY() + object.getImgHeight() >= camera.position.y - halfHeight;
	}
	
	public void followTarget() {
		currentUpdateBehavior = normalUpdateBehavior;
	}
	
	public void noMovement() {
		currentUpdateBehavior = staticUpdateBehavior;
	}
	
	private static interface UpdateBehavior {
		void update(float delta);
	}
	
	private class NormalUpdateBehavior implements UpdateBehavior {
		@Override
		public void update(float delta) {
			position.y = 
					MathUtils.lerp(position.y, (target.getCenter().y - BIRD_VISION_OFFSET), FOLLOW_SPEED * delta);
					//position.lerp(target.getCenter(), FOLLOW_SPEED * delta);
		}
	}
	
	private static class StaticUpdateBehavior implements UpdateBehavior{
		@Override
		public void update(float delta) {}
	}
	
	public float getWidthPlusOffset() {
		return camera.viewportWidth + 2 * VIEW_OFFSET;
	}
	
	public float getHeightPlusOffset() {
		return camera.viewportHeight + 2 * VIEW_OFFSET;
	}
	
	public float getNorthEdgePosition() {
		return position.y + (camera.viewportHeight / 2f + VIEW_OFFSET);
	}
	
	public float getSouthEdgePosition() {
		return position.y - (camera.viewportHeight / 2f + VIEW_OFFSET);
	}
}
