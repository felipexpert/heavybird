package br.com.geppetto.heavybird.util.command;

import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;

import com.badlogic.gdx.Gdx;

public class ExitFromGame extends Command<Object> {
	private final UpdateBehavior onCommand = new OnCommand();
	
	@Override
	public void execute(Object type) {
		SoundManager.instance.play(Sound.QUIT);
		onCommand.reset();
		behavior = onCommand;
	}
	
	private class OnCommand implements UpdateBehavior {
		private float time;
		private static final float MAX_TIME = 1.375f;
		@Override
		public void update(float delta) {
			time += delta;
			if(time >= MAX_TIME) {
				Gdx.app.exit();
				behavior = NullUpdateBehavior.instance;
			}
		}
		@Override
		public void reset() {
			time = 0f;
		}
	}
}
