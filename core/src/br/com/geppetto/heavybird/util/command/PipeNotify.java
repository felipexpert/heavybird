package br.com.geppetto.heavybird.util.command;

import br.com.geppetto.heavybird.gameObject.pipe.PipesManager;

public class PipeNotify extends Command<Object> {	
	private final PipesManager manager;
	
	private final PipeNotifyBehavior pipeNotifyBehavior
		= new PipeNotifyBehavior(); 
	
	public PipeNotify(PipesManager manager) {
		this.manager = manager;
	}
	
	@Override
	public void execute(Object type) {
		pipeNotifyBehavior.reset();
		behavior = pipeNotifyBehavior;
	}
	
	private class PipeNotifyBehavior implements UpdateBehavior {
		private float time;
		private static final float MAX = .25f;
		@Override
		public void update(float delta) {
			time += delta;
			if(time >= MAX) {
				manager.nextLevel();
				time = 0f;
				behavior = NullUpdateBehavior.instance;
			}
		}
		
		@Override
		public void reset() {
			time = 0f;
		}
	}

}
