package br.com.geppetto.heavybird.util.command;

public class NullUpdateBehavior implements UpdateBehavior{
	public static final NullUpdateBehavior instance = new NullUpdateBehavior();
	
	private NullUpdateBehavior() {}
	
	@Override
	public void update(float delta) {}

	@Override
	public void reset() {}
}
