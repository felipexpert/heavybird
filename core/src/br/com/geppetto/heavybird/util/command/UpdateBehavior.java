package br.com.geppetto.heavybird.util.command;

public interface UpdateBehavior {
	void update(float delta);
	void reset();
}
