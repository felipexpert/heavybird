package br.com.geppetto.heavybird.util.command;

public class PipeUpdateGracefully extends Command<InfoWrapper>{

	private final CommandBehavior cBehavior = new CommandBehavior();
	
	private InfoWrapper info;

	@Override
	public void execute(InfoWrapper type) {
		info = type;
		behavior = cBehavior;
		cBehavior.currPassageDistance = info.initialPassageDistance;
		cBehavior.increase = info.targetPassageDistance > info.initialPassageDistance;
	}
	
	private class CommandBehavior implements UpdateBehavior {
		private float currPassageDistance;
		private boolean increase;
		private static final float DEFAULT_ADJUST_VELOCITY = 4f;
		@Override
		public void update(float delta) {
			currPassageDistance += (increase ? 1f : -1f) * delta * DEFAULT_ADJUST_VELOCITY;
			if((increase && currPassageDistance >= info.targetPassageDistance) || 
			   (!increase && currPassageDistance <= info.targetPassageDistance)) {
				currPassageDistance = info.targetPassageDistance;
				info.array.removeValue(PipeUpdateGracefully.this, true);
			}
			info.pipe.updatePassage(currPassageDistance);
		}

		@Override
		public void reset() {}
		
	}
}
