package br.com.geppetto.heavybird.util.command;

import br.com.geppetto.heavybird.controller.AdsIntervalHelper;
import br.com.geppetto.heavybird.screen.manager.ScreenManager;
import br.com.geppetto.heavybird.screen.manager.ScreenType;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;

public class ExitToMenu extends Command<AdsIntervalHelper>{
	private final UpdateBehavior onCommand = new OnCommand();
	@Override
	public void execute(AdsIntervalHelper type) {
		type.increaseTime();
		SoundManager.instance.play(Sound.QUIT);
		onCommand.reset();
		behavior = onCommand;
	}
	
	private class OnCommand implements UpdateBehavior {
		private float time;
		private static final float MAX_TIME = 1.375f;
		@Override
		public void update(float delta) {
			time += delta;
			if(time >= MAX_TIME) {
				ScreenManager.instance.setScreen(ScreenType.MENU);
				behavior = NullUpdateBehavior.instance;
			}
		}
		
		@Override
		public void reset() {
			time = 0f;
		}
	}

}
