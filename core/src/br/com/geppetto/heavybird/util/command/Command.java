package br.com.geppetto.heavybird.util.command;

public abstract class Command <T> {
	protected UpdateBehavior behavior = NullUpdateBehavior.instance;
	abstract public void execute(T type);
	
	public void update(float delta) {
		behavior.update(delta);
	}
	
	public boolean executing() {
		return behavior != NullUpdateBehavior.instance;
	}
	
	public void stop() {
		behavior.reset();
		behavior = NullUpdateBehavior.instance;
	}
}
