package br.com.geppetto.heavybird.util.command;

import br.com.geppetto.heavybird.gameObject.pipe.Pipe;

import com.badlogic.gdx.utils.Array;

public class InfoWrapper {
	public final Pipe pipe;
	public final float targetPassageDistance;
	public final float initialPassageDistance;
	final Array<PipeUpdateGracefully> array;
	
	public InfoWrapper(Pipe pipe, float targetPassageDistance, float initialPassageDistance, Array<PipeUpdateGracefully> array) {
		this.pipe = pipe;
		this.targetPassageDistance = targetPassageDistance;
		this.initialPassageDistance = initialPassageDistance;
		this.array = array;
	}
}

