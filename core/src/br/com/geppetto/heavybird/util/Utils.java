package br.com.geppetto.heavybird.util;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Utils {
	private Utils() {}
	
	public static float distance(float p1x, float p1y, float p2x, float p2y) {
		float p1 = p1x - p2x;
		float p2 = p1y - p2y;
		return (float)Math.sqrt(p1 * p1 + p2 * p2);
	}
	
	public static float getAngle(Vector2 p1, Vector2 p2) {
		//float angle = (float) Math.toDegrees(Math.atan2(p2.y - p1.y, p2.x - p1.x));
		
		return MathUtils.radiansToDegrees * MathUtils.atan2(p2.y - p1.y, p2.x - p1.x);
	}
	
	public static void getPosition(float fromX, float fromY, float angle, float distance, Vector2 answer) {
		float radians = angle * MathUtils.degreesToRadians;
        //Get SOH
        float op = MathUtils.sin(radians) * distance;
        //Get CAH
        float ad = MathUtils.cos(radians) * distance;
        
        //Add to from Vector
        answer.x = fromX + ad;
        answer.y = fromY + op;
    }
	
	public static void updateRelativelyCenter(float fromX, float fromY, float angle, float distance, AbstractGameObject target) {
		float radians = angle * MathUtils.degreesToRadians;
        //Get SOH
        float op = MathUtils.sin(radians) * distance;
        //Get CAH
        float ad = MathUtils.cos(radians) * distance;
        
        target.updateCenterX(fromX + ad);
        target.updateCenterY(fromY + op);
	}
}
