package br.com.geppetto.heavybird.gameObject;

import br.com.geppetto.heavybird.asset.RegionWrapper;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class NonAnimatedDrawableHelper implements DrawableHelper {

	private RegionWrapper wrapper;
	
	
	public NonAnimatedDrawableHelper() {}
	
	public NonAnimatedDrawableHelper(RegionWrapper wrapper) {
		this.wrapper = wrapper;
	}
	
	public void setAtlasRegion(RegionWrapper atlasRegion) {
		this.wrapper = atlasRegion;
	}
	
	@Override
	public TextureRegion getTextureRegion() {
		return wrapper.getRegion();
	}

	@Override
	public void setTime(float time) {}

	@Override
	public void addTime(float delta) {}

	@Override
	public boolean isAnimationFinished() {
		return false;
	}
}
