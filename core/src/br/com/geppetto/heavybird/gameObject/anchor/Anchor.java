package br.com.geppetto.heavybird.gameObject.anchor;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.bird.Bird;
import br.com.geppetto.heavybird.util.Utils;

public class Anchor {
	
	private static final float MAX_INCLINATION_RIGHT = 15f;
	private static final float MAX_INCLINATION_LEFT = 345f;
	//private static final float INCLINATION_VELOCITY = 5f;
	
	private final AbstractGameObject anchor = GameObjectFactory
			.instance.getGameObject(GameObject.ANCHOR);
	
	private final float distance = anchor.getHeight() / 2f;
	
	private float rotation = 0f;
	
	private final Bird bird;
	
	public Anchor(Bird bird) {
		this.bird = bird;
	}
	
	public void right() {
		anchor.setFlipX(false);
	}
	
	public void left() {
		anchor.setFlipX(true);
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		anchor.debugRender(shapeRenderer);
	}
	
	public void render(SpriteBatch batch) {
		anchor.render(batch);
	}
	
	private void adjustRotation(AbstractGameObject target) {
		float inclination = rotation + 270f;
		
		Utils.updateRelativelyCenter(target.getCenter().x, 
				target.getCenter().y - target.getImgHeight() / 2f, inclination, distance, anchor);
		anchor.addPositionY(-.125f);
		anchor.setImgRotation(rotation);
		
	}
	
	public void positionCenter(AbstractGameObject target) {
		rotation = 0f;
		adjustRotation(target);
	}
	
	public void positionRight(AbstractGameObject target) {
		bird.speedy();
		rotation = MAX_INCLINATION_RIGHT;
		adjustRotation(target);
	}
	
	public void positionLeft(AbstractGameObject target) {
		bird.speedy();
		rotation = MAX_INCLINATION_LEFT;
		adjustRotation(target);
	}
	
	public AbstractGameObject getObject() {
		return anchor;
	}
}
