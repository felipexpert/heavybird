package br.com.geppetto.heavybird.gameObject.cloud;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.util.CameraHelper;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.Utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class Clouds {
	private  float horizontalVelocity;
	private static final int MAX_INIT_LOOP = 1000;
	private static final int MAX_INTEGRATION_LOOP = 25;
	private int loops;
	private final float MIN_DISTANCE = 9.3f;
	private final Array<AbstractGameObject> clouds = new Array<AbstractGameObject>();
	private AbstractGameObject outCloud;
	private final CameraHelper cameraHelper;
	
	private float timeToChange;
	private float time;
	private boolean right;
	
	public Clouds(CameraHelper cameraHelper) {
		this.cameraHelper = cameraHelper;
		init();
	}
	
	private float genValidX() {
		return MathUtils.random(-5f, Constants.VIEWPORT_WIDTH + 1f);
	}
	
	private float genValidY() {
		return MathUtils.random(-5f, Constants.VIEWPORT_HEIGHT + 7f);
	}
	
	private float genValidTryY() {
		float south = cameraHelper.getSouthEdgePosition();
		float max = south - 5f;
		float min = south - (Constants.VIEWPORT_HEIGHT + 17f);
		return MathUtils.random(min, max);
	}
	
	private void genTimeToChangeDirection() {
		timeToChange = 10f + MathUtils.random(-5f, 5f);
	}
	
	private void genHorizontalVelocity() {
		horizontalVelocity = 3f + MathUtils.random(-1.5f, 1.5f);
	}
	
	public void init() {
		if(clouds.size == 0) {
			A:
			while(loops < MAX_INIT_LOOP) {
				loops++;
				float validX = genValidX();
				float validY = genValidY();
				for(AbstractGameObject c : clouds) {
					if(Utils.distance(validX, validY, c.getImgX(), c.getImgY()) < MIN_DISTANCE) {
						continue A;
					} 
				}
				AbstractGameObject c1 = GameObjectFactory.instance.getGameObject(GameObject.CLOUD);
				c1.updatePosition(validX, validY);
				clouds.add(c1);
			}
		} else {				
			for(AbstractGameObject c : clouds) {
				c.updatePosition(999f, 999f);
			}
			int curr = 0;
			A:
			while(loops < MAX_INIT_LOOP) {
				loops++;
				float validX = genValidX();
				float validY = genValidY();
				for(AbstractGameObject c : clouds) {
					if(Utils.distance(validX, validY, c.getImgX(), c.getImgY()) < MIN_DISTANCE) {
						continue A;
					} 
				}
				AbstractGameObject c1 = clouds.get(curr);
				c1.updatePosition(validX, validY);
				curr++;
				if(curr == clouds.size) break A;
			}
		}
		loops = 0;
		
		genHorizontalVelocity();
		genTimeToChangeDirection();
	}
	
	private void tryToDeliver() {
		
		if(outCloud == null) return;
		
		float validX = 0f;
		float validY = 0f;
		A:
		for(int i = 0; i < MAX_INTEGRATION_LOOP; i++) {
			validX = genValidX();
			validY = genValidTryY();
			for(AbstractGameObject c : clouds) {
				if(Utils.distance(validX, validY, c.getImgX(), c.getImgY()) < MIN_DISTANCE) {
					continue A;
				} 
			}
			outCloud.updatePosition(validX, validY);
			outCloud = null;
			return;
		}
	}
	
	public void update(float delta) {
		for(AbstractGameObject c : clouds) {
			if(c.getImgY() > cameraHelper.getNorthEdgePosition()) {
				if(outCloud == null) outCloud = c;
			} else {
				c.updatePositionX(c.getImgX() + (right ? 1f : -1f) * delta * horizontalVelocity);
			}
		}
		tryToDeliver();
		
		time += delta;
		if(time >= timeToChange) {
			right = !right;
			genTimeToChangeDirection();
			time = 0f;
		}
		if(time >= timeToChange / 4f) {
			genHorizontalVelocity();
		}
	}
	
	public void render(SpriteBatch batch) {
		for(AbstractGameObject c : clouds) {
			c.render(batch);
		}
	}
}
