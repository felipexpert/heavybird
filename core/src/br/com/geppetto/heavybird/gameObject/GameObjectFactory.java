package br.com.geppetto.heavybird.gameObject;

import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.math.Vector2;

public class GameObjectFactory {
	
	public static final GameObjectFactory instance = new GameObjectFactory();
	
	public final Vector2 _0X0 = new Vector2(0f, 0f);
	public final Vector2 _1X1 = new Vector2(1f, 1f);
	public final float birdWidth = 1.75f;
	public final Vector2 BIRD_SIZE = new Vector2(birdWidth, birdWidth * (14f / 18f));
	public final Vector2 BIRD_ORIGIN = new Vector2(BIRD_SIZE.x / 2f, BIRD_SIZE.y / 2f);
	public final Vector2 BIRD_BOUNDS_OFFSET = new Vector2(.375f, .25f);
	public final AnimatedDrawableHelper BIRD_MENU_DRAWABLE_HELPER =
			new AnimatedDrawableHelper();
	public final float anchorWidth = 1.5f;
	public final Vector2 ANCHOR_SIZE = new Vector2(anchorWidth, anchorWidth * (15f / 16f));
	public final Vector2 ANCHOR_ORIGIN = new Vector2(ANCHOR_SIZE.x / 2f, ANCHOR_SIZE.y / 2f);
	public final Vector2 ANCHOR_BOUNDS_OFFSET = new Vector2(.2f, .185f);
	public final NonAnimatedDrawableHelper ANCHOR_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 WALL_SIZE = _1X1;
	public final Vector2 WALL_ORIGIN = new Vector2(WALL_SIZE.x / 2f, WALL_SIZE.y / 2f);
	public final Vector2 WALL_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper WALL_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 PIPE_SIZE = new Vector2(Constants.VIEWPORT_WIDTH, 2.25f);
	public final Vector2 PIPE_ORIGIN = new Vector2(PIPE_SIZE.x / 2, PIPE_SIZE.y / 2);
	public final Vector2 PIPE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper PIPE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 PIPE_EDGE_SIZE = new Vector2(1.375f, PIPE_SIZE.y * 1.125f);
	public final Vector2 PIPE_EDGE_ORIGIN = new Vector2(PIPE_SIZE.x / 2, PIPE_SIZE.y / 2);
	public final Vector2 PIPE_EDGE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper PIPE_EDGE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float hardcoreWidth = 6f;
	public final Vector2 HARDCORE_SIZE = new Vector2(hardcoreWidth, hardcoreWidth * (30f / 86f));
	public final Vector2 HARDCORE_ORIGIN = _0X0;
	public final Vector2 HARDCORE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper HARDCORE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float easyWidth = 4f;
	public final Vector2 EASY_SIZE = new Vector2(easyWidth, easyWidth * (30f / 54f));
	public final Vector2 EASY_ORIGIN = _0X0;
	public final Vector2 EASY_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper EASY_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float okWidth = 4.25f; 
	public final Vector2 OK_SIZE = new Vector2(okWidth, okWidth * (30f/64f));
	public final Vector2 OK_ORIGIN = _0X0;
	public final Vector2 OK_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper OK_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 SOUND_SIZE = new Vector2(1.75f, 1.25f);
	public final Vector2 SOUND_ORIGIN = _0X0;
	public final Vector2 SOUND_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper SOUND_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 SCORE_SIZE = new Vector2(2.75f, .875f);
	public final Vector2 SCORE_ORIGIN = _0X0;
	public final Vector2 SCORE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper SCORE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 BEST_SIZE = new Vector2(2.25f, .875f);
	public final Vector2 BEST_ORIGIN = _0X0;
	public final Vector2 BEST_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper BEST_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 SCORE_BACKGROUND_SIZE = new Vector2(11.25f, 8.5f);
	public final Vector2 SCORE_BACKGROUND_ORIGIN = _0X0;
	public final Vector2 SCORE_BACKGROUND_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper SCORE_BACKGROUND_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 RESUME_SIZE = new Vector2(3.5f, 1.75f);
	public final Vector2 RESUME_ORIGIN = _0X0;
	public final Vector2 RESUME_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper RESUME_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float exitWidth = 1.375f;
	public final Vector2 EXIT_SIZE = new Vector2(exitWidth, exitWidth * (115f / 59f));
	public final Vector2 EXIT_ORIGIN = _0X0;
	public final Vector2 EXIT_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper EXIT_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 ARROW_SIZE = new Vector2(Constants.VIEWPORT_WIDTH / 2f, 2.5f);
	public final Vector2 ARROW_ORIGIN = _0X0;
	public final Vector2 ARROW_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper ARROW_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	
	public final Vector2 HAND_RIGHT_SIZE = new Vector2(2f, 2.125f);
	public final Vector2 HAND_RIGHT_ORIGIN = _0X0;
	public final Vector2 HAND_RIGHT_BOUNDS_OFFSET = _0X0;
	public final AnimatedDrawableHelper HAND_RIGHT_DRAWABLE_HELPER = 
			new AnimatedDrawableHelper();
	public final Vector2 HAND_LEFT_SIZE = new Vector2(2f, 2.125f);
	public final Vector2 HAND_LEFT_ORIGIN = _0X0;
	public final Vector2 HAND_LEFT_BOUNDS_OFFSET = _0X0;
	public final AnimatedDrawableHelper HAND_LEFT_DRAWABLE_HELPER = 
			new AnimatedDrawableHelper();
	public final float cloudWidth = 5.5f;
	public final Vector2 CLOUD_SIZE = new Vector2(cloudWidth, cloudWidth * (18f / 40f));
	public final Vector2 CLOUD_ORIGIN = new Vector2(CLOUD_SIZE.x / 2f, CLOUD_SIZE.y / 2f);
	public final Vector2 CLOUD_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper CLOUD_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 MEDAL_POSITION = new Vector2();
	public final Vector2 MEDAL_SIZE = new Vector2(3f, 3f);
	public final Vector2 MEDAL_ORIGIN = _0X0;
	public final Vector2 MEDAL_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper PLATINUM_MEDAL_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final NonAnimatedDrawableHelper GOLD_MEDAL_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final NonAnimatedDrawableHelper SILVER_MEDAL_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final NonAnimatedDrawableHelper BRONZE_MEDAL_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float logoWidth = Constants.VIEWPORT_WIDTH;
	public final Vector2 LOGO_SIZE = new Vector2(logoWidth, logoWidth * (113f / 382f));
	public final Vector2 LOGO_ORIGIN = _0X0;
	public final Vector2 LOGO_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper LOGO_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	
	public final float getReadyWidth = 12f;
	public final Vector2 GET_READY_SIZE = new Vector2(getReadyWidth, getReadyWidth * (24f / 154f));
	public final Vector2 GET_READY_ORIGIN = _0X0;
	public final Vector2 GET_READY_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper GET_READY_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float gameOverWidth = 9f;
	public final Vector2 GAME_OVER_SIZE = new Vector2(gameOverWidth, gameOverWidth * (24f / 154f));
	public final Vector2 GAME_OVER_ORIGIN = _0X0;
	public final Vector2 GAME_OVER_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper GAME_OVER_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float groundGreenWidth = 1.25f;
	public final Vector2 GROUND_GREEN_SIZE = new Vector2(groundGreenWidth, groundGreenWidth * (22f / 24f));
	public final Vector2 GROUND_GREEN_ORIGIN = _0X0;
	public final Vector2 GROUND_GREEN_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper GROUND_GREEN_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 GROUND_BROWN_ORIGIN = _0X0;
	public final Vector2 GROUND_BROWN_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper GROUND_BROWN_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float leaderboardWidth = 3f;
	public final Vector2 LEADERBOARD_SIZE = new Vector2(leaderboardWidth, leaderboardWidth * (58f / 104f));
	public final Vector2 LEADERBOARD_ORIGIN = _0X0;
	public final Vector2 LEADERBOARD_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper LEADERBOARD_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final Vector2 SHARE_SIZE = LEADERBOARD_SIZE;
	public final Vector2 SHARE_ORIGIN = _0X0;
	public final Vector2 SHARE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper SHARE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	public final float rateWidth = 9f;
	public final Vector2 RATE_SIZE = new Vector2(rateWidth, rateWidth * (24f / 154f));
	public final Vector2 RATE_ORIGIN = _0X0;
	public final Vector2 RATE_BOUNDS_OFFSET = _0X0;
	public final NonAnimatedDrawableHelper RATE_DRAWABLE_HELPER = 
			new NonAnimatedDrawableHelper();
	private GameObjectFactory() {
		init();
	}
	
	private void init() {
		BIRD_MENU_DRAWABLE_HELPER.setAnimation(Assets.instance.bird.birdMenuWrapper);
		ANCHOR_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.anchor.anchorWrapper);
		WALL_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.wall.wallWrapper);
		PIPE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.pipe.pipeWrapper);
		PIPE_EDGE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.pipe.pipeEdgeWrapper);
		HARDCORE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.hardcoreWrapper);
		EASY_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.easyWrapper);
		OK_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.okWrapper);
		SOUND_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.soundWrapper);
		SCORE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.scoreWrapper);
		BEST_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.bestWrapper);
		SCORE_BACKGROUND_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.scoreBackgroundWrapper);
		RESUME_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.resumeWrapper);
		EXIT_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.exitWrapper);
		ARROW_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.arrowWrapper);
		HAND_RIGHT_DRAWABLE_HELPER.setAnimation(Assets.instance.menuItem.handRightWrapper);
		HAND_LEFT_DRAWABLE_HELPER.setAnimation(Assets.instance.menuItem.handLeftWrapper);
		CLOUD_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.cloud.cloudWrapper);
		PLATINUM_MEDAL_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.medalPlatinumWrapper);
		GOLD_MEDAL_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.medalGoldWrapper);
		SILVER_MEDAL_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.medalSilverWrapper);
		BRONZE_MEDAL_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.medalBronzeWrapper);
		LOGO_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.bird.logoWrapper);
		GET_READY_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.getReadyWrapper);
		GAME_OVER_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.gameOverWrapper);
		GROUND_GREEN_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.groundGreen);
		GROUND_BROWN_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.groundBrown);
		LEADERBOARD_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.leaderBoard);
		SHARE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.share);
		RATE_DRAWABLE_HELPER.setAtlasRegion(Assets.instance.menuItem.rate);
	}
	
	public AbstractGameObject getGameObject(GameObject gameObject) {
		AbstractGameObject go = null;
		switch (gameObject) {
		case BIRD:
			go = new AbstractGameObject(new Vector2(), BIRD_SIZE, BIRD_ORIGIN, _1X1, BIRD_BOUNDS_OFFSET);
			break;
		case ANCHOR:
			go = new AbstractGameObject(ANCHOR_DRAWABLE_HELPER, new Vector2(), ANCHOR_SIZE, ANCHOR_ORIGIN, _1X1, ANCHOR_BOUNDS_OFFSET);
			break;
		case PIPE:
			go = new AbstractGameObject(PIPE_DRAWABLE_HELPER, new Vector2(), PIPE_SIZE, PIPE_ORIGIN, _1X1, PIPE_BOUNDS_OFFSET);
			break;
		case PIPE_EDGE:
			go = new AbstractGameObject(PIPE_EDGE_DRAWABLE_HELPER, new Vector2(), PIPE_EDGE_SIZE, PIPE_EDGE_ORIGIN, _1X1, PIPE_EDGE_BOUNDS_OFFSET);
			break;
		case WALL:
			go = new SimpleAbstractGameObject(WALL_DRAWABLE_HELPER, new Vector2(), WALL_SIZE, WALL_ORIGIN, _1X1, WALL_BOUNDS_OFFSET);
			break;
		case RIGHT_ARROW:
			go = new SimpleAbstractGameObject(ARROW_DRAWABLE_HELPER, new Vector2(), ARROW_SIZE, ARROW_ORIGIN, _1X1, ARROW_BOUNDS_OFFSET);
			break;
		case LEFT_ARROW:
			go = new SimpleAbstractGameObject(ARROW_DRAWABLE_HELPER, new Vector2(), ARROW_SIZE, ARROW_ORIGIN, _1X1, ARROW_BOUNDS_OFFSET);
			go.setFlipX(true);
			break;
		case EASY:
			go = new AbstractGameObject(EASY_DRAWABLE_HELPER, new Vector2(), EASY_SIZE, EASY_ORIGIN, _1X1, HARDCORE_BOUNDS_OFFSET);
			break;
		case HARDCORE:
			go = new AbstractGameObject(HARDCORE_DRAWABLE_HELPER, new Vector2(), HARDCORE_SIZE, HARDCORE_ORIGIN, _1X1, HARDCORE_BOUNDS_OFFSET);
			break;
		case OK:
			go = new AbstractGameObject(OK_DRAWABLE_HELPER, new Vector2(), OK_SIZE, OK_ORIGIN, _1X1, OK_BOUNDS_OFFSET);
			break;
		case SOUND:
			go = new AbstractGameObject(SOUND_DRAWABLE_HELPER, new Vector2(), SOUND_SIZE, SOUND_ORIGIN, _1X1, SOUND_BOUNDS_OFFSET);
			break;
		case LEADERBOARD:
			go = new AbstractGameObject(LEADERBOARD_DRAWABLE_HELPER, new Vector2(), LEADERBOARD_SIZE, LEADERBOARD_ORIGIN, _1X1, LEADERBOARD_BOUNDS_OFFSET);
			break;
		case SHARE:
			go = new AbstractGameObject(SHARE_DRAWABLE_HELPER, new Vector2(), SHARE_SIZE, SHARE_ORIGIN, _1X1, SHARE_BOUNDS_OFFSET);
			break;
		case SCORE:
			go = new AbstractGameObject(SCORE_DRAWABLE_HELPER, new Vector2(), SCORE_SIZE, SCORE_ORIGIN, _1X1, SCORE_BOUNDS_OFFSET);
			break;
		case RATE:
			go = new AbstractGameObject(RATE_DRAWABLE_HELPER, new Vector2(), RATE_SIZE, RATE_ORIGIN, _1X1, RATE_BOUNDS_OFFSET);
			break;
		case BEST:
			go = new AbstractGameObject(BEST_DRAWABLE_HELPER, new Vector2(), BEST_SIZE, BEST_ORIGIN, _1X1, BEST_BOUNDS_OFFSET);
			break;
		case SCORE_BACKGROUND:
			go = new AbstractGameObject(SCORE_BACKGROUND_DRAWABLE_HELPER, new Vector2(), SCORE_BACKGROUND_SIZE, SCORE_BACKGROUND_ORIGIN, _1X1, SCORE_BACKGROUND_BOUNDS_OFFSET);
			break;
		case RESUME:
			go = new AbstractGameObject(RESUME_DRAWABLE_HELPER, new Vector2(), RESUME_SIZE, RESUME_ORIGIN, _1X1, RESUME_BOUNDS_OFFSET);
			break;
		case EXIT:
			go = new AbstractGameObject(EXIT_DRAWABLE_HELPER, new Vector2(), EXIT_SIZE, EXIT_ORIGIN, _1X1, EXIT_BOUNDS_OFFSET);
			break;
		case HAND_RIGHT:
			go = new SimpleAbstractGameObject(HAND_RIGHT_DRAWABLE_HELPER, new Vector2(), HAND_RIGHT_SIZE, HAND_RIGHT_ORIGIN, _1X1, HAND_RIGHT_BOUNDS_OFFSET);
			break;
		case HAND_LEFT:
			go = new SimpleAbstractGameObject(HAND_LEFT_DRAWABLE_HELPER, new Vector2(), HAND_LEFT_SIZE, HAND_LEFT_ORIGIN, _1X1, HAND_LEFT_BOUNDS_OFFSET);
			break;
		case PLATINUM:
			go = new SimpleAbstractGameObject(PLATINUM_MEDAL_DRAWABLE_HELPER, MEDAL_POSITION, MEDAL_SIZE, MEDAL_ORIGIN, _1X1, MEDAL_BOUNDS_OFFSET);
			break;
		case GOLD:
			go = new SimpleAbstractGameObject(GOLD_MEDAL_DRAWABLE_HELPER, MEDAL_POSITION, MEDAL_SIZE, MEDAL_ORIGIN, _1X1, MEDAL_BOUNDS_OFFSET);
			break;
		case SILVER:
			go = new SimpleAbstractGameObject(SILVER_MEDAL_DRAWABLE_HELPER, MEDAL_POSITION, MEDAL_SIZE, MEDAL_ORIGIN, _1X1, MEDAL_BOUNDS_OFFSET);
			break;
		case BRONZE:
			go = new SimpleAbstractGameObject(BRONZE_MEDAL_DRAWABLE_HELPER, MEDAL_POSITION, MEDAL_SIZE, MEDAL_ORIGIN, _1X1, MEDAL_BOUNDS_OFFSET);
			break;
		case CLOUD:
			go = new SimpleAbstractGameObject(CLOUD_DRAWABLE_HELPER, new Vector2(), CLOUD_SIZE, CLOUD_ORIGIN, _1X1, CLOUD_BOUNDS_OFFSET);
			break;
		case CLOUD_MENU:
			go = new AbstractGameObject(CLOUD_DRAWABLE_HELPER, new Vector2(), CLOUD_SIZE, CLOUD_ORIGIN, _1X1, CLOUD_BOUNDS_OFFSET);
			break;
		case GET_READY:
			go = new AbstractGameObject(GET_READY_DRAWABLE_HELPER, new Vector2(), GET_READY_SIZE, GET_READY_ORIGIN, _1X1, GET_READY_BOUNDS_OFFSET);
			break;
		case GAME_OVER:
			go = new AbstractGameObject(GAME_OVER_DRAWABLE_HELPER, new Vector2(), GAME_OVER_SIZE, GAME_OVER_ORIGIN, _1X1, GAME_OVER_BOUNDS_OFFSET);
			break;
		case BIRD_MENU:
			go = new AbstractGameObject(BIRD_MENU_DRAWABLE_HELPER, new Vector2(), BIRD_SIZE, BIRD_ORIGIN, _1X1, BIRD_BOUNDS_OFFSET);
			break;
		case LOGO:
			go = new AbstractGameObject(LOGO_DRAWABLE_HELPER, new Vector2(), LOGO_SIZE, LOGO_ORIGIN, _1X1, LOGO_BOUNDS_OFFSET);
			break;
		case GROUND_GREEN:
			go = new SimpleAbstractGameObject(GROUND_GREEN_DRAWABLE_HELPER, new Vector2(), GROUND_GREEN_SIZE, GROUND_GREEN_ORIGIN, _1X1, GROUND_GREEN_BOUNDS_OFFSET);
			break;
		case GROUND_BROWN:
			go = new SimpleAbstractGameObject(GROUND_BROWN_DRAWABLE_HELPER, new Vector2(), new Vector2(), GROUND_BROWN_ORIGIN, _1X1, GROUND_BROWN_BOUNDS_OFFSET);
			break;
		}
		return go;
	}
}
