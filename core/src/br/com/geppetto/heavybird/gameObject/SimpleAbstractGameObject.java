package br.com.geppetto.heavybird.gameObject;

import com.badlogic.gdx.math.Vector2;

public class SimpleAbstractGameObject extends AbstractGameObject{

	public SimpleAbstractGameObject(Vector2 imgPosition, Vector2 imgInWorldSize, Vector2 imgOrigin,
			Vector2 imgScale, Vector2 boundsOffset) {
		super(imgPosition, imgInWorldSize, imgOrigin, imgScale,
				boundsOffset);
	}

	public SimpleAbstractGameObject(DrawableHelper drawableHelper,
			Vector2 imgPosition, Vector2 imgInWorldSize, Vector2 imgOrigin,
			Vector2 imgScale, Vector2 boundsOffset) {
		super(drawableHelper, imgPosition, imgInWorldSize, imgOrigin, imgScale,
				boundsOffset);
	}

	@Override
	public void updatePosition(float x, float y) {
		imgPosition.x = x;
		imgPosition.y = y;
	}
}
