package br.com.geppetto.heavybird.gameObject.wall;

import br.com.geppetto.heavybird.controller.controller.WorldController;
import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.bird.Bird;
import br.com.geppetto.heavybird.gameObject.util.BiController;
import br.com.geppetto.heavybird.util.CameraHelper;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class Walls {
	private final Array<AbstractGameObject> walls = new Array<AbstractGameObject>();
	
	private final BiController biController = new BiController();
	
	private final CameraHelper cameraHelper;
	
	public Walls(CameraHelper cameraHelper) {
		this.cameraHelper = cameraHelper;
		float leftWalls = 0f;
		float rightWalls = Constants.VIEWPORT_WIDTH - GameObjectFactory.instance.WALL_SIZE.x;
		for(int i = 0; i < Constants.VIEWPORT_HEIGHT + 20; i++) {
			AbstractGameObject wallLeft = GameObjectFactory.instance.getGameObject(GameObject.WALL);
			wallLeft.updatePosition(leftWalls, i);
			
			AbstractGameObject wallRight = GameObjectFactory.instance.getGameObject(GameObject.WALL);
			wallRight.updatePosition(rightWalls, i);
			walls.add(wallLeft);
			walls.add(wallRight);
		}
		resetValues();
	}
	
	private void resetValues() {
		biController.init(walls);
	}
	
	public void init(float y) {
		int count = 0;
		int position = (int)y;
		for(AbstractGameObject w : walls) {
			w.updatePositionY(position);
			count++;
			if(count == 2) {
				count = 0;
				position++;
			}
		}
		resetValues();
	}
	
	public void update() {
		//int lastWallLeft = walls.size - 2;
		boolean stop = false;
		while(!stop) {
			AbstractGameObject wallLeft = walls.get(biController.getLastLeft());
			if(!cameraHelper.isUnderVision(wallLeft)) {
				//int lastWallRight = walls.size - 1;
				AbstractGameObject wallRight = walls.get(biController.getLastRight());
				
				float y = walls.get(biController.getFirstLeft()).getY() - 
						walls.get(biController.getFirstLeft()).getHeight();
				wallLeft.updatePositionY(y);
				wallRight.updatePositionY(y);
				
				biController.next();
			} else {
				stop = true;
			}
		}
	}
	
	public void update2() {
		int lastWallLeft = walls.size - 2;
		AbstractGameObject wallLeft = walls.get(lastWallLeft);
		if(!cameraHelper.isUnderVision(wallLeft)) {
			int lastWallRight = walls.size - 1;
			AbstractGameObject wallRight = walls.get(lastWallRight);
			
			float y = walls.get(0).getY() - walls.get(0).getHeight();
			wallLeft.updatePositionY(y);
			wallRight.updatePositionY(y);
			walls.removeIndex(lastWallRight);
			walls.removeIndex(lastWallLeft);
			walls.insert(0, wallRight);
			walls.insert(0, wallLeft);
		}
	}
	
	public void render(SpriteBatch batch) {
		for(AbstractGameObject w : walls) {
			w.render(batch);
		}
	}
	
	public void testBirdWallsColision(Bird bird, WorldController controller) {
		if(bird.getBirdObject().getX() < 
		   GameObjectFactory.instance.WALL_SIZE.x ||
		   bird.getBirdObject().getX() + bird.getBirdObject().getWidth() >
		   Constants.VIEWPORT_WIDTH - 
		   GameObjectFactory.instance.WALL_SIZE.x) {
				if(bird.die()) {
					controller.onDeath();
				}
		}
	}
}
