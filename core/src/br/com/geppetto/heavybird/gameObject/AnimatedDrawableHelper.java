package br.com.geppetto.heavybird.gameObject;

import br.com.geppetto.heavybird.asset.AnimationWrapper;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedDrawableHelper implements DrawableHelper{

	private AnimationWrapper wrapper;
	private float time;
	
	public AnimatedDrawableHelper() {}
	
	public AnimatedDrawableHelper(AnimationWrapper wrapper) {
		this.wrapper = wrapper;
	}
	
	public void setAnimation(AnimationWrapper animation) {
		this.wrapper = animation;
	}
	
	@Override
	public TextureRegion getTextureRegion() {
		return wrapper.getAnimation().getKeyFrame(time);
	}

	@Override
	public void setTime(float time) {
		this.time = time;
	}

	@Override
	public void addTime(float delta) {
		time += delta;
	}


	@Override
	public boolean isAnimationFinished() {
		return wrapper.getAnimation().isAnimationFinished(time);
	}

}
