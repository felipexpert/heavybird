package br.com.geppetto.heavybird.gameObject.menuitem;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;

public enum MenuItem {
	EASY(GameObject.EASY),
	HARDCORE(GameObject.HARDCORE), 
	OK(GameObject.OK),
	SOUND(GameObject.SOUND),
	SCORE(GameObject.SCORE),
	BEST(GameObject.BEST),
	SCORE_BACKGROUND(GameObject.SCORE_BACKGROUND),
	RESUME(GameObject.RESUME), 
	EXIT(GameObject.EXIT),
	CLOUD_MENU(GameObject.CLOUD_MENU),
	LOGO(GameObject.LOGO),
	LEADERBOARD(GameObject.LEADERBOARD),
	SHARE(GameObject.SHARE),
	RATE(GameObject.RATE);
	
	private final GameObject object;
	
	MenuItem(GameObject object) {
		this.object = object;
	}
	
	public AbstractGameObject generateItem() {
		return GameObjectFactory.instance.getGameObject(object);
	}
}
