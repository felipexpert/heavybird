package br.com.geppetto.heavybird.gameObject.menuitem;

import br.com.geppetto.heavybird.sound.SoundManager;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SoundButton extends GenericMenuItem{
	
	public SoundButton() {
		super(MenuItem.SOUND);
	}
	
	@Override
	public void render(SpriteBatch batch) {
		if(!SoundManager.instance.isPlayable())
			batch.setColor(1f, 1f, 1f, .5f);
		super.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}
	
	public void toggle() {
		SoundManager.instance.toggle();
	}
}
