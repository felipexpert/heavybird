package br.com.geppetto.heavybird.gameObject.menuitem;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class GenericMenuItem {
	private AbstractGameObject gameObject;
	
	public GenericMenuItem() {}
	
	public GenericMenuItem(MenuItem item) {
		this.gameObject = item.generateItem();
	}
	
	public void updatePosition(float x, float y) {
		gameObject.updatePosition(x, y);
	}
	
	public void updatePositionX(float x) {
		gameObject.updatePositionX(x);
	}
	
	public void updatePositionY(float y) {
		gameObject.updatePositionY(y);
	}
	
	public void addPosition(float x, float y) {
		gameObject.addPosition(x, y);
	}
	
	public void addPositionX(float x) {
		gameObject.addPositionX(x);
	}
	
	public void addPositionY(float y) {
		gameObject.addPositionY(y);
	}
	
	public float getWidth() {
		return gameObject.getWidth();
	}
	
	public float getHeight() {
		return gameObject.getHeight();
	}
	
	public float getX() {
		return gameObject.getX();
	}
	
	public float getY() {
		return gameObject.getY();
	}
	
	public float getCenterX() {
		return gameObject.getCenter().x;
	}
	
	public float getCenterY() {
		return gameObject.getCenter().y;
	}
	
	public void setCenterX(float x) {
		gameObject.updateCenterX(x);
	}
	
	public void setCenterY(float y) {
		gameObject.updateCenterY(y);
	}
	
	public void setX(float x) {
		gameObject.updatePositionX(x);
	}
	
	public void setY(float y) {
		gameObject.updatePositionY(y);
	}
	
	public void setSize(float width, float height) {
		gameObject.setImgSize(width, height);
	}
	
	public void setButton(MenuItem item) {
		this.gameObject = item.generateItem();
	}
	
	public boolean wasClicked(float x, float y) {
		return gameObject.isInBounds(x, y);
	}
	
	public boolean wasClicked(Vector2 point) {
		return wasClicked(point.x, point.y);
	}
	
	public void render(SpriteBatch batch) {
		gameObject.render(batch);
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		gameObject.debugRender(shapeRenderer);
	}
}
