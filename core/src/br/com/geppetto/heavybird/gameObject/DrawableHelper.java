package br.com.geppetto.heavybird.gameObject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public interface DrawableHelper {
	TextureRegion getTextureRegion();
	void setTime(float time);
	void addTime(float delta);
	boolean isAnimationFinished();
}
