package br.com.geppetto.heavybird.gameObject.util;

import com.badlogic.gdx.utils.Array;

public class BiController {
	private int firstLeft;
	private int firstRight;
	private int lastLeft;
	private int lastRight;
	
	private Array<?> collection;
	
	public BiController(int firstLeft, int firstRight, int lastLeft,
			int lastRight, Array<?> collection) {
		super();
		init(firstLeft, firstRight, lastLeft, lastRight, collection);
	}
	
	public BiController() {}
	
	public void init(Array<?> collection) {
		firstLeft = 0; firstRight = 1;
		lastLeft = collection.size - 2; lastRight = collection.size - 1;
		this.collection = collection;
	}
	
	public void init(int firstLeft, int firstRight, int lastLeft,
			int lastRight, Array<?> collection) {
		this.firstLeft = firstLeft;
		this.firstRight = firstRight;
		this.lastLeft = lastLeft;
		this.lastRight = lastRight;
		this.collection = collection;
	}
	
	public void next() {
		firstLeft = lastLeft;
		firstRight = lastRight;
		lastLeft -= 2;
		lastRight -= 2;
		if(lastLeft < 0) 
			lastLeft = collection.size + lastLeft;
		if(lastRight < 0)
			lastRight = collection.size + lastRight;
	}

	public int getFirstLeft() {
		return firstLeft;
	}

	public int getFirstRight() {
		return firstRight;
	}

	public int getLastLeft() {
		return lastLeft;
	}

	public int getLastRight() {
		return lastRight;
	}

	public Array<?> getCollection() {
		return collection;
	}
}
