package br.com.geppetto.heavybird.gameObject.bird;

import br.com.geppetto.heavybird.controller.ModeManager;
import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.anchor.Anchor;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

public class Bird {
	private float fallVelocity;
	private float horizontalVelocity;
	private final BirdHelper helper = new BirdHelper();
	
	private final float g ;
	private final float minVelocity;
	private static final float HORIZONTAL_MOVEMENT = 4f;
	private static final float VERTICAL_MOVEMENT = 6f;
	//private static final float RANDOM_MIN = -1.2f;
	//private static final float RANDOM_MAX = 1.9f;
	private static final float RANDOM_MIN = -1.75f;
	private static final float RANDOM_MAX = 1.75f;
	private static final float MIN_TIME_LAST_COMMAND = .0625f;
	private static final float TERMINAL = -50f;
	
	private static final float SPEEDY_VELOCITY = HORIZONTAL_MOVEMENT * 1.555f;
	//private final AbstractGameObject anchor = GameObjectFactory.instance.getGameObject(GameObject.ANCHOR);
	private final Anchor anchor = new Anchor(this);
	private float timeLastCommand = 99f;
	
	
	private boolean alive;
	
	public Bird() {
		g = ModeManager.INSTANCE.currentMode.g;
		minVelocity = ModeManager.INSTANCE.currentMode.minVelocity;
	}
	
	
	public void init() {
		alive = true;
		helper.bird.updatePosition(Constants.VIEWPORT_WIDTH / 2f, 0f);
		float factor = 0;
		if(MathUtils.randomBoolean()) {
			flipRight();
			factor = 1f;
		} else {
			flipLeft();
			factor = -1f;
		}
		
		horizontalVelocity = factor * HORIZONTAL_MOVEMENT / 3f;
		fallVelocity = 0f;
		adjustAnchor(0f);
	}
	
	public void speedy() {
		helper.speedy();
	}
	
	public void update(float delta) {
		timeLastCommand += delta;
		if(fallVelocity < g)
			helper.fall();
		helper.updateAnimation(delta);
		fall(delta);
		helper.bird.addPosition(horizontalVelocity * delta, fallVelocity * delta);
		adjustAnchor(delta);
	}
	
	private boolean canAct() {
		if(timeLastCommand >= MIN_TIME_LAST_COMMAND) {
			timeLastCommand = 0f;
			return true;
		}
		return false;
	}
	
	private void flipRight() {
		helper.bird.setFlipX(false);
		anchor.right();
	}
	
	private void goRight() {
		flipRight();
		playerActionUp();
		playerHorizontalAction(true);
	}

	public void actRight() {
		if(canAct()) {
			helper.fly();
			goRight();
			playWingSound();
		}
	}

	private void flipLeft() {
		helper.bird.setFlipX(true);
		anchor.left();
	}
	
	private void goLeft() {
		flipLeft();
		playerActionUp();
		playerHorizontalAction(false);
	}

	public void actLeft() {
		if(canAct()) {
			helper.fly();
			goLeft();
			playWingSound();
		}
	}
	
	private void playWingSound() {
		if(Math.abs(horizontalVelocity) <= SPEEDY_VELOCITY) {
			SoundManager.instance.play(Sound.WING);
		} else {
			SoundManager.instance.play(Sound.WING_SPEEDY);
		}
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		anchor.debugRender(shapeRenderer);
		helper.bird.debugRender(shapeRenderer);
	}
	
	public void render(SpriteBatch batch) {
		helper.bird.render(batch);
		anchor.render(batch);
	}

	private void playerHorizontalAction(boolean right) {
		if(right && horizontalVelocity < 0 || !right && horizontalVelocity > 0)
			horizontalVelocity = 0f;
		horizontalVelocity += (right ? 1f : -1f) * (HORIZONTAL_MOVEMENT + MathUtils.random(RANDOM_MIN, RANDOM_MAX));
	}

	private void fall(float delta) {
		fallVelocity += g * delta;
		fallVelocity = Math.max(fallVelocity, TERMINAL);
	}

	private void playerActionUp() {
		fallVelocity = Math.min((fallVelocity * .875f) + VERTICAL_MOVEMENT, minVelocity);
	}

	private void adjustAnchor(float delta) {
		/*if(fallVelocity < -15f) {
			anchor.positionCenter(bird);
		} else {
			if(bird.getFlipX()) {
				anchor.positionRight(bird);
			} else {
				anchor.positionLeft(bird);
			}
		}*/
		if(Math.abs(horizontalVelocity) <= SPEEDY_VELOCITY) {
			anchor.positionCenter(helper.bird);
		} else if(horizontalVelocity > 0) {
			anchor.positionLeft(helper.bird);
		} else {
			anchor.positionRight(helper.bird);
		}
		//anchor.putAnchorBelow(bird);
		
		//anchor.updateCenterX(bird.getCenter().x);
	}
	
	public AbstractGameObject getBirdObject() {
		return helper.bird;
	}
	
	public AbstractGameObject getAnchorObject() {
		return anchor.getObject();
	}
	
	public float getCenterY() {
		return helper.bird.getCenter().y;
	}
	
	public boolean die() {
		if(alive) {
			alive = false;
			horizontalVelocity = 0f;
			return true;
		}
		return false;
	}
	
	public boolean isAlive() {
		return alive;
	}
}
