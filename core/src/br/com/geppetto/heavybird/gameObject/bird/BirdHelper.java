package br.com.geppetto.heavybird.gameObject.bird;

import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.AnimatedDrawableHelper;
import br.com.geppetto.heavybird.gameObject.DrawableHelper;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.NonAnimatedDrawableHelper;

public class BirdHelper {
	final AbstractGameObject bird = GameObjectFactory.instance.getGameObject(GameObject.BIRD);
	private DrawableHelper birdDefault =  new NonAnimatedDrawableHelper(Assets.instance.bird.birdWrapper);
	private DrawableHelper birdFly = new AnimatedDrawableHelper(Assets.instance.bird.birdFlyWrapper);
	private DrawableHelper birdFall = new AnimatedDrawableHelper(Assets.instance.bird.birdFallWrapper);
	private DrawableHelper birdSpeedy = new NonAnimatedDrawableHelper(Assets.instance.bird.birdSpeedyWrapper);
	
	{
		bird.setDrawableHelper(birdDefault);
	}
	
	void updateAnimation(float delta) {
		bird.updateAnimation(delta);
	}
	
	void fly() {
		bird.resetAnimation();
		bird.setDrawableHelper(birdFly);
	}
	
	void fall() {
		if((bird.getDrawableHelper() != birdFly && !isFalling()) 
				|| (bird.getDrawableHelper() == birdFly && bird.isAnimationFinished())) {
			bird.resetAnimation();
			bird.setDrawableHelper(birdFall);
		}
	}
	
	void speedy() {
		bird.resetAnimation();
		bird.setDrawableHelper(birdSpeedy);
	}
	
	boolean isFalling() {
		return bird.getDrawableHelper() == birdFall;
	}
}
