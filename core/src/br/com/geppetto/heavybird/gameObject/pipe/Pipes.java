package br.com.geppetto.heavybird.gameObject.pipe;

import br.com.geppetto.heavybird.controller.controller.WorldController;
import br.com.geppetto.heavybird.gameObject.bird.Bird;
import br.com.geppetto.heavybird.util.CameraHelper;
import br.com.geppetto.heavybird.util.command.InfoWrapper;
import br.com.geppetto.heavybird.util.command.PipeUpdateGracefully;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Pipes {
	private static final String TAG = Pipes.class.getSimpleName();
	
	private final WorldController controller;
	
	private final Bird bird;
	
	private final PipesManager manager;
	
	private final Array<Pipe> pipes = new Array<Pipe>();
	
	private final CameraHelper cameraHelper;
	
	final StaticPipeBehavior staticPipeBehavior = new StaticPipeBehavior();
	
	final CloseMovingPipeBehavior closeMovingPipeBehavior = new CloseMovingPipeBehavior();
	
	final HorizontalMovingPipeBehavior horizontalMovingPipeBehavior = new HorizontalMovingPipeBehavior();
	
	final HorizontalAndCloseMovingPipeBehavior horizontalAndCloseMovingPipeBehavior = new HorizontalAndCloseMovingPipeBehavior();
	
	PipeBehavior currentPipeBehavior;
	
	final Vector2 pipeHorizontalEdges = new Vector2();
	
	private int lastPipeCode = -1;
	
	private final Array<PipeUpdateGracefully> updateGracefully = new Array<PipeUpdateGracefully>();
	
	public Pipes(WorldController controller, Bird bird, Array<PipeType> pipeTypes, CameraHelper cameraHelper) {
		this.controller = controller;
		this.bird = bird;
		manager = new PipesManager(this, pipeTypes);
		this.cameraHelper = cameraHelper;
		manager.init();
		pipes.add(generateNewPipe(-20f));
	}
	
	public void init() {
		manager.init();
		lastPipeCode = -1;
		boolean first = true;
		for(Pipe p : pipes) {
			if(first) {
				useExistingPipe(p, -20f);
				first = false;
			} else {
				useExistingPipe(p, 99f);
			}
		}
	}
	
	public void update(float delta) {
		for(PipeUpdateGracefully g : updateGracefully) {
			g.update(delta);
		}
		manager.update(delta);
		currentPipeBehavior.update(delta);
	}
	
	private void useExistingPipe(Pipe pipe, float y) {
		pipe.updatePosition(y);
		pipe.setCode(Pipe.generateCode());
		pipe.setRandomPassagePosition(manager.getPipeType().passageDistance);
	}
	
	private Pipe generateNewPipe(float y) {
		Pipe p = new Pipe(pipeHorizontalEdges, cameraHelper);
		p.updatePosition(y);
		p.setCode(Pipe.generateCode());
		p.setRandomPassagePosition(manager.getPipeType().passageDistance);
		return p;
	}
	
	void gameOver() {
		Gdx.app.debug(TAG, "Game Over");
		controller.init();
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		for(Pipe pipe : pipes) {
			pipe.debugRender(shapeRenderer);
		}
	}
	
	public void render(SpriteBatch batch) {
		for(Pipe pipe : pipes) {
			pipe.render(batch);
		}
	}
	
	private interface PipeBehavior {
		void update(float delta);
	}
	
	public void arrangePassage() {
		for(Pipe p : pipes) {
			if(p.getCode() > lastPipeCode) {
				if(!manager.getPipeType().close) {
					InfoWrapper w = new InfoWrapper(p, manager.getPipeType().passageDistance, p.getCurrPassageDistance(), updateGracefully);
					PipeUpdateGracefully pg = new PipeUpdateGracefully();
					pg.execute(w);
					updateGracefully.add(pg);
					//p.updatePassage(manager.getPipeType().passageDistance);
				} else {
					p.close();
				}
			}
		}
	}
	
	private class StaticPipeBehavior implements PipeBehavior {
		@Override
		public void update(float delta) {
			adjustPipes();
			Pipe nearest = null;
			float difference = 1f;
			{ // Find nearest
				float distance = 999f;
				for(Pipe p : pipes) {
					float df = bird.getCenterY() - p.getCenterY();
					float d = Math.abs(df);
					if(d < distance) {
						nearest = p;
						distance = d;
						difference = df;
					}
				}
			}
			if(nearest.colided(bird)) {
				if(bird.die()) {
					controller.onDeath();
				}
				return;
			}
			if(nearest.getCode() > lastPipeCode) {
				if(difference <= 0f) {
					lastPipeCode = nearest.getCode();
					if(bird.isAlive())
						manager.increase();
				}
			}
		}
		
		private void adjustPipes() {
			Pipe down = pipes.first();
			for(Pipe p : pipes) {
				if(p.getCenterY() < down.getCenterY())
					down = p;
			}
			if(down.isUnderVision()) {
				Pipe up = null;
				float y = down.getCenterY() - manager.getPipeType().distance;
				for(Pipe p : pipes) {
					if(!p.isUnderVision()) {
						up = p;
						break;
					}
				}
				
				if(up != null) {
					useExistingPipe(up, y);
				} else {
					pipes.add(generateNewPipe(y));
				}
			}
		}
	}
	
	private class CloseMovingPipeBehavior implements PipeBehavior {
		@Override
		public void update(float delta) {
			staticPipeBehavior.update(delta);
			act(delta);
		}
		
		public void act(float delta) {
			Pipe nearest = null;
			{ // Find nearest
				float distance = 999f;
				for(Pipe p : pipes) {
					float df = bird.getCenterY() - p.getCenterY();
					float d = Math.abs(df);
					if(d < distance) {
						nearest = p;
						distance = d;
					}
				}
			}
			nearest.openCloseMove(manager.getPipeType().passageDistance,
					delta,
					manager.getPipeType().closeVel);
			/*for(Pipe pipe : pipes) {
				pipe.openCloseMove(manager.getPipeType().passageDistance,
						delta,
						manager.getPipeType().closeVel);
			}*/
		}
	}
	
	private class HorizontalMovingPipeBehavior implements PipeBehavior{
		@Override
		public void update(float delta) {
			staticPipeBehavior.update(delta);
			act(delta);
		}
		
		public void act(float delta) {
			for(Pipe pipe : pipes) {
				float verticalDistance = Math.abs(bird.getCenterY() - pipe.getCenterY());  
				if(verticalDistance <= manager.getPipeType().notifyDistance) {
					pipe.horizontalMove(delta,
							manager.getPipeType().moveVel);
				}
			}
		}
	}
	
	private class HorizontalAndCloseMovingPipeBehavior implements PipeBehavior {
		@Override
		public void update(float delta) {
			staticPipeBehavior.update(delta);
			horizontalMovingPipeBehavior.act(delta);
			closeMovingPipeBehavior.act(delta);
		}
		
	}
	
	public void informPoints(int points) {
		controller.informPoints(points);
	}
}
