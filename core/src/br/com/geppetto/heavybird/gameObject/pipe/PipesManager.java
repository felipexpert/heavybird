package br.com.geppetto.heavybird.gameObject.pipe;

import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.command.PipeNotify;

import com.badlogic.gdx.utils.Array;

public class PipesManager {
	private final Array<PipeType> pipeTypes;
	
	private int currentPipeType;
	
	private final Pipes pipes;
	
	private int points;
	
	private int lastPointChange;
	
	private final PipeNotify pipeNotify;
	
	public PipesManager(Pipes pipes, Array<PipeType> pipeTypes) {
		this.pipeTypes = pipeTypes;
		this.pipes = pipes;
		pipeNotify = new PipeNotify(this);
	}	
	
	public void init() {
		lastPointChange = 0;
		points = 0;
		currentPipeType = -1;
		nextLevel();
	}
	
	public void increase() {
		SoundManager.instance.play(Sound.POINT);
		points++;
		pipes.informPoints(points);
		if(points == lastPointChange + pipeTypes.get(currentPipeType).nextLevel) {
			lastPointChange = points;
			pipeNotify.execute(null);
		}
	}
	
	public void nextLevel() {
		currentPipeType++;
		if(currentPipeType == pipeTypes.size) {
			pipes.gameOver();
			return;
		}
		if(pipeTypes.get(currentPipeType).move && pipeTypes.get(currentPipeType).close)
			pipes.currentPipeBehavior = pipes.horizontalAndCloseMovingPipeBehavior;
		else if(pipeTypes.get(currentPipeType).close)
			pipes.currentPipeBehavior = pipes.closeMovingPipeBehavior;
		else if(pipeTypes.get(currentPipeType).move)
			pipes.currentPipeBehavior = pipes.horizontalMovingPipeBehavior;
		else
			pipes.currentPipeBehavior = pipes.staticPipeBehavior;
		
		// Set passage configurations
		{
			float difference = 
					GameObjectFactory.instance.PIPE_EDGE_SIZE.x + .125f
					+ GameObjectFactory.instance.WALL_SIZE.x; 
			pipes.pipeHorizontalEdges.x = difference; 
				
			pipes.pipeHorizontalEdges.y =
					Constants.VIEWPORT_WIDTH - difference - 
					pipeTypes.get(currentPipeType).passageDistance;
		}
		
		// arrange already placed pipes
		pipes.arrangePassage();
	}
	
	public PipeType getPipeType() {
		return pipeTypes.get(currentPipeType);
	}
	
	public int getPoints() {
		return points;
	}
	
	public void update(float delta) {
		pipeNotify.update(delta);
	}
}
