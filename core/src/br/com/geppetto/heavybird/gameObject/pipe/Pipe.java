package br.com.geppetto.heavybird.gameObject.pipe;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.bird.Bird;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.CameraHelper;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Pipe { 
	
	private static int lastCode = -1;
	
	private final Vector2 pipeHorizontalEdges;
	
	private final CameraHelper cameraHelper;
	
	private boolean horizontalMoveRight;
	
	private boolean close;
	
	private float xPassagePosition; 
	
	private int code;
	
	private final AbstractGameObject pipeLeft = 
			GameObjectFactory.instance.getGameObject(GameObject.PIPE);
	private final AbstractGameObject pipeLeftEdge =
			GameObjectFactory.instance.getGameObject(GameObject.PIPE_EDGE);
	private final AbstractGameObject pipeRight =
			GameObjectFactory.instance.getGameObject(GameObject.PIPE);
	private final AbstractGameObject pipeRightEdge=
			GameObjectFactory.instance.getGameObject(GameObject.PIPE_EDGE);
	{
		pipeRight.setFlipX(true);
		pipeRightEdge.setFlipX(true);
	}
	
	public Pipe(Vector2 pipeHorizontalEdges, CameraHelper cameraHelper) {
		this.pipeHorizontalEdges = pipeHorizontalEdges;
		this.cameraHelper = cameraHelper;
	}
	
	public void updatePosition(float y) {
		pipeLeft.updateCenterY(y);
		pipeLeftEdge.updateCenterY(y);
		pipeRight.updateCenterY(y);
		pipeRightEdge.updateCenterY(y);
	}
	
	private void setHorizontalPassagePosition(float x, float passageDistance) {
		this.xPassagePosition = x;
		updatePassage(passageDistance);
	}
	
	public void setRandomPassagePosition(float passageDistance) {
		horizontalMoveRight = MathUtils.randomBoolean();
		float x = MathUtils.random(pipeHorizontalEdges.x,
				pipeHorizontalEdges.y);
		setHorizontalPassagePosition(x, passageDistance);
	}
	
	public void updatePassage(float passageDistance) {
		float x = (Constants.VIEWPORT_WIDTH - GameObjectFactory.instance.WALL_SIZE.x + .25f) - (xPassagePosition + passageDistance);
		if(x < 0) {
			xPassagePosition -= x;
		}
		pipeLeft.updatePositionX(xPassagePosition - pipeLeft.getWidth());
		pipeLeftEdge.updatePositionX(xPassagePosition - pipeLeftEdge.getWidth());
		float rightPosition = xPassagePosition + passageDistance;
		pipeRight.updatePositionX(rightPosition);
		pipeRightEdge.updatePositionX(rightPosition);
	}
	
	public float getCurrPassageDistance() {
		return pipeRightEdge.getX() - (pipeLeftEdge.getX() + pipeLeftEdge.getWidth());
	}
	
	public void close() {
		close = true;
	}
	
	public void openCloseMove(float passageDistance, float delta, float velocity) {
		float curr = getCurrPassageDistance();
		float factor = (close ? -8f : 1f) * velocity * delta;
		xPassagePosition += factor / -2f;
		curr += factor * 3f / 2f;
		if(curr <= 0f) {
			SoundManager.instance.play(Sound.PIPE_COLISION);
			curr = 0f + curr;
			close = false;
		} else if(curr >= passageDistance) {
			curr = passageDistance - (curr - passageDistance);
			close = true;
		}
		updatePassage(curr);
	}

	public void horizontalMove(float deltaTime,float velocity) {
		float newX = 0f;
		float passageDistance = getCurrPassageDistance();
		if(horizontalMoveRight) {
			newX = xPassagePosition + (velocity * deltaTime);
			if(newX > pipeHorizontalEdges.y) {
				newX = pipeHorizontalEdges.y;
				horizontalMoveRight = false;
			}
		} else {
			newX = xPassagePosition - (velocity * deltaTime);
			if(newX < pipeHorizontalEdges.x) {
				newX = pipeHorizontalEdges.x;
				horizontalMoveRight = true;
			}
		}
		setHorizontalPassagePosition(newX, passageDistance);
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		pipeLeft.debugRender(shapeRenderer);
		pipeLeftEdge.debugRender(shapeRenderer);
		pipeRight.debugRender(shapeRenderer);
		pipeRightEdge.debugRender(shapeRenderer);
	}
	
	public void render(SpriteBatch batch) {
		pipeLeft.render(batch);
		pipeLeftEdge.render(batch);
		pipeRight.render(batch);
		pipeRightEdge.render(batch);
	}
	
	public boolean isUnderVision() {
		return cameraHelper.isUnderVision(pipeLeftEdge);
	}
	
	public float getCenterY() {
		return pipeLeft.getCenter().y;
	}
	
	public static int generateCode() {
		return ++lastCode;
	}
	
	public boolean colided(Bird bird) {
		return pipeLeftEdge.colided(bird.getBirdObject()) ||
				pipeLeftEdge.colided(bird.getAnchorObject()) ||
				pipeLeft.colided(bird.getBirdObject()) ||
				pipeLeft.colided(bird.getAnchorObject()) ||
				pipeRightEdge.colided(bird.getBirdObject()) ||
				pipeRightEdge.colided(bird.getAnchorObject()) ||
				pipeRight.colided(bird.getBirdObject()) ||
				pipeRight.colided(bird.getAnchorObject());
	}
}
