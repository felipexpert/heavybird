package br.com.geppetto.heavybird.gameObject.pipe;

public class PipeType {
	public final float passageDistance; 
	public final float distance;
	public final boolean move;
	public final float moveVel;
	public final float notifyDistance;
	public final boolean close;
	public final float closeVel;
	public final int nextLevel;
	
	/**
	 * Static pipes
	 * @param passageDistance
	 * @param distance
	 * @param nextLevel
	 */
	public PipeType(float passageDistance, float distance, int nextLevel) {
		this.passageDistance = passageDistance;
		this.distance = distance;
		this.moveVel = 0f;
		this.notifyDistance = 0f;
		closeVel = 0f;
		move = false;
		close = false;
		this.nextLevel = nextLevel;
	}
	
	/**
	 * Close and open pipes
	 * @param passageDistance
	 * @param distance
	 * @param closeVel
	 * @param nextLevel
	 */
	public PipeType(float passageDistance, float distance, float closeVel, int nextLevel) {
		this.passageDistance = passageDistance;
		this.distance = distance;
		moveVel = 0f;
		notifyDistance = 0f;
		this.closeVel = closeVel;
		move = false;
		close = true;
		this.nextLevel = nextLevel;
	}
	
	/**
	 * Horizontal moving pipes
	 * @param passageDistance
	 * @param distance
	 * @param moveVel
	 * @param notifyDistance
	 * @param nextLevel
	 */
	public PipeType(float passageDistance, float distance, float moveVel, float notifyDistance, int nextLevel) {
		this.passageDistance = passageDistance;
		this.distance = distance;
		this.moveVel = moveVel;
		this.notifyDistance = notifyDistance;
		closeVel = 0f;
		move = true;
		close = false;
		this.nextLevel = nextLevel;
	}
	
	/**
	 * Horizontal moving and close/open pipes
	 * @param passageDistance
	 * @param distance
	 * @param closeVel
	 * @param moveVel
	 * @param notifyDistance
	 * @param nextLevel
	 */
	public PipeType(float passageDistance, float distance, float closeVel, float moveVel, float notifyDistance, int nextLevel) {
		this.passageDistance = passageDistance;
		this.distance = distance;
		this.moveVel = moveVel;
		this.notifyDistance = notifyDistance;
		this.closeVel = closeVel;
		move = true;
		close = true;
		this.nextLevel = nextLevel;
	}
}
