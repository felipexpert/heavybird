package br.com.geppetto.heavybird.gameObject.symboliccontrolls;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.util.Constants;

public class SymbolicControll {
	private final AbstractGameObject rightArrow = GameObjectFactory.instance.getGameObject(GameObject.RIGHT_ARROW);
	private final AbstractGameObject leftArrow = GameObjectFactory.instance.getGameObject(GameObject.LEFT_ARROW);
	
	private static final float MAX_TIME = 0.25f;
	
	private float rightTime;
	private float leftTime;
	
	{
		leftArrow.updatePosition(0f, 0f);
		rightArrow.updatePosition(Constants.VIEWPORT_WIDTH / 2f, 0f);
	}
	
	public void update(float delta) {
		rightTime = Math.max(rightTime - delta, 0f);
		leftTime = Math.max(leftTime - delta, 0f);
	}
	
	public void pressRight() {
		rightTime = MAX_TIME;
	}
	
	public void pressLeft() {
		leftTime = MAX_TIME;
	}
	
	public void render(SpriteBatch batch) {
		if(leftTime == 0f)
			batch.setColor(1f, 1f, 1f, .5f);
		leftArrow.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
		if(rightTime == 0f)
			batch.setColor(1f, 1f, 1f, .5f);
		rightArrow.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}
}
