package br.com.geppetto.heavybird.gameObject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class AbstractGameObject {
	
	protected DrawableHelper drawableHelper;
	
	protected final Vector2 imgPosition;
	private final Vector2 imgInWorldSize;
	private final Vector2 imgOrigin;
	private final Vector2 imgScale;
	private final Vector2 boundsOffset;
	private float imgRotation;
	private boolean imgFlipX = false;
	private boolean imgFlipY = false;
	
	private final Rectangle bounds = new Rectangle();
	private final Vector2 center = new Vector2();
	
	public AbstractGameObject(Vector2 imgPosition, Vector2 imgInWorldSize, Vector2 imgOrigin,
			Vector2 imgScale, Vector2 boundsOffset) {
		this(null, imgPosition, imgInWorldSize, imgOrigin, imgScale, boundsOffset);
	}
	
	public AbstractGameObject(DrawableHelper drawableHelper, Vector2 imgPosition, Vector2 imgInWorldSize, Vector2 imgOrigin,
			Vector2 imgScale, Vector2 boundsOffset) {
		this.drawableHelper = drawableHelper;
		this.imgPosition = imgPosition;
		this.imgInWorldSize = imgInWorldSize;
		this.imgOrigin = imgOrigin;
		this.imgScale = imgScale;
		this.boundsOffset = boundsOffset;
		
		initBounds();
	}
	
	public void debugRender(ShapeRenderer shapeRenderer) {
		shapeRenderer.rect(bounds.x, bounds.y, bounds.width, bounds.height);
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(drawableHelper.getTextureRegion().getTexture(),
				imgPosition.x, imgPosition.y, 
				imgOrigin.x, imgOrigin.y, 
				imgInWorldSize.x, imgInWorldSize.y, 
				imgScale.x,
				imgScale.y, 
				imgRotation, 
				drawableHelper.getTextureRegion().getRegionX(),
				drawableHelper.getTextureRegion().getRegionY(), 
				drawableHelper.getTextureRegion().getRegionWidth(),
				drawableHelper.getTextureRegion().getRegionHeight(), 
				imgFlipX, imgFlipY);
	}
	
	public void setImgSize(float width, float height) {
		imgInWorldSize.x = width;
		imgInWorldSize.y = height;
		initBounds();
	}
	
	private void initBounds() {
		bounds.width = imgInWorldSize.x - (boundsOffset.x * 2f);
		bounds.height = imgInWorldSize.y - (boundsOffset.y * 2f);
		center.x = imgPosition.x + imgOrigin.x;
		center.y = imgPosition.y + imgOrigin.y;
	}
	
	public void updatePositionX(float x) {
		bounds.x = x;
		imgPosition.x = x - boundsOffset.x;
		center.x = imgPosition.x + imgOrigin.x;
	}
	
	public void updateCenterX(float x) {
		center.x = x;
		bounds.x = x - (bounds.width / 2);
		imgPosition.x = bounds.x - boundsOffset.x;
	}
	
	public void updateCenterY(float y) {
		center.y = y;
		bounds.y = y - (bounds.height / 2);
		imgPosition.y = bounds.y - boundsOffset.y;
	}
	
	public void updatePositionY(float y) {
		bounds.y = y;
		imgPosition.y = y - boundsOffset.y;
		center.y = imgPosition.y + imgOrigin.y;
	}
	
	public void updatePosition(float x, float y) {
		updatePositionX(x);
		updatePositionY(y);
	}
	
	public void addPositionX(float x) {
		updatePositionX(getX() + x);
	}
	
	public void addPositionY(float y) {
		updatePositionY(getY() + y);
	}
	
	public void addPosition(float x, float y) {
		addPositionX(x);
		addPositionY(y);
	}
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	public float getX() {
		return bounds.x;
	}
	
	public float getImgX() {
		return imgPosition.x;
	}
	
	public float getY() {
		return bounds.y;
	}
	
	public float getImgY() {
		return imgPosition.y;
	}
	
	public Vector2 getCenter() {
		return center;
	}
	
	public float getWidth() {
		return bounds.width;
	}
	
	public float getImgWidth() {
		return imgInWorldSize.x;
	}
	
	public float getHeight() {
		return bounds.height;
	}
	
	public float getImgHeight() {
		return imgInWorldSize.y;
	}
	
	public void setDrawableHelper(DrawableHelper drawableHelper) {
		this.drawableHelper = drawableHelper;
	}
	
	public DrawableHelper getDrawableHelper() {
		return drawableHelper;
	}
	
	public void resetAnimation() {
		drawableHelper.setTime(0f);
	}
	
	public void updateAnimation(float delta) {
		drawableHelper.addTime(delta);
	}
	
	public boolean isAnimationFinished() {
		return drawableHelper.isAnimationFinished();
	}
	
	public boolean colided(AbstractGameObject object) {
		return bounds.overlaps(object.bounds);
	}
	
	public boolean isInBounds(float x, float y) {
		return x >= bounds.x &&
			   x <= (bounds.x + bounds.width) &&
			   y >= bounds.y &&
			   y <= (bounds.y + bounds.height);
	}
	
	public void setFlipX(boolean b) {
		imgFlipX = b;
	}
	
	public void setFlipY(boolean b) {
		imgFlipY = b;
	}
	
	public boolean getFlipX() {
		return imgFlipX;
	}
	
	public float getImgRotation() {
		return imgRotation;
	}
	
	public void setImgRotation(float rotation) {
		this.imgRotation = rotation;
	}
}
