package br.com.geppetto.heavybird.sound;

import br.com.geppetto.heavybird.asset.Assets;

public class SoundManager {
	public static final SoundManager instance = new SoundManager();
	
	private boolean playable;
	
	private SoundManager(){
		playable = true;
	}
	
	public void play(Music music) {
		if(playable) {
			switch (music) {
			case THEME:
				Assets.instance.musics.theme.play();
				break;
			}
		}
	}
	
	public void playTheme() {
		Assets.instance.musics.theme.play();
	}
	
	public void stopTheme() {
		Assets.instance.musics.theme.stop();
	}
	
	public boolean isThemePlaying() {
		return Assets.instance.musics.theme.isPlaying();
	}
	
	public void play(Sound sound) {
		if(playable) {
			switch (sound) {
			case WING:
				Assets.instance.sounds.wing.play();
				break;
			case WING_SPEEDY:
				Assets.instance.sounds.wingSpeedy.play();
				break;
			case POINT:
				Assets.instance.sounds.point.play();
				break;
			case HIT:
				Assets.instance.sounds.hit.play();
				break;
			case FALLING:
				Assets.instance.sounds.falling.play();
				break;
			case PIPE_COLISION:
				Assets.instance.sounds.pipeColision.play();
				break;
			case PLAY:
				Assets.instance.sounds.play.play();
				break;
			case QUIT:
				Assets.instance.sounds.quit.play();
				break;
			}
		}
	}
	
	public void setPlayable(boolean playable) {
		this.playable = playable;
	}
	
	public void toggle() {
		playable = !playable;
	}
	
	public boolean isPlayable() {
		return playable;
	}
}
