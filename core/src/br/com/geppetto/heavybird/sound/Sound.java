package br.com.geppetto.heavybird.sound;

public enum Sound {
	WING, WING_SPEEDY, POINT, HIT, FALLING, PLAY, QUIT, PIPE_COLISION;
}
