package br.com.geppetto.heavybird.screen;

import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.sound.SoundManager;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;

public abstract class AbstractGameScreen implements Screen, Initializable{
	public abstract void render (float deltaTime);

	public abstract void resize (int width, int height);

	public abstract void show ();

	public abstract void hide ();

	public void pause () {
		SoundManager.instance.stopTheme();
	}

	public abstract InputProcessor getInputProcessor ();

	public void resume () {
		Assets.instance.init(new AssetManager());
	}

	public void dispose () {
		Assets.instance.dispose();
	}
}
