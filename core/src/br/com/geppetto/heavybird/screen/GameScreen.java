package br.com.geppetto.heavybird.screen;

import br.com.geppetto.heavybird.controller.controller.WorldController;
import br.com.geppetto.heavybird.renderer.WorldRenderer;
import br.com.geppetto.heavybird.util.Constants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class GameScreen extends AbstractGameScreen {
	
	private final WorldRenderer worldRenderer;
	private final WorldController worldController;
	
	private final RenderBehavior normal = new NormalRenderBehavior();
	private final RenderBehavior paused = new PausedRenderBehavior();
	private RenderBehavior currentBehavior;

	public GameScreen() {
		OrthographicCamera camera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		
		worldController = new WorldController(camera);
		worldRenderer = new WorldRenderer(worldController, camera);
		//worldRenderer = new DebugWorldRenderer(worldController, camera);
	}

	@Override
	public void render(float deltaTime) {
		currentBehavior.render(deltaTime);
	}

	@Override
	public void resize(int width, int height) {
		worldRenderer.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
		currentBehavior = paused;
	}
	
	@Override
	public void show() {
		currentBehavior = normal;
	}

	@Override
	public void hide() {
		worldRenderer.dispose();
	}
	
	@Override
	public void resume() {
		super.resume();
		currentBehavior = normal;
	}

	@Override
	public void dispose() {
		worldRenderer.dispose();
		super.dispose();
	}

	@Override
	public InputProcessor getInputProcessor() {
		return worldController;
	}
	
	private static interface RenderBehavior {
		void render(float deltaTime);
	}
	
	private void renderWorld() {
		Gdx.gl.glClearColor(WorldController.clearGameColor.r,
				WorldController.clearGameColor.g,
				WorldController.clearGameColor.b,
				WorldController.clearGameColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		worldRenderer.render();
	}	
	
	private class NormalRenderBehavior implements RenderBehavior{
		@Override
		public void render(float deltaTime) {
			worldController.update(deltaTime);
			
			renderWorld();
		}
	}
	private class PausedRenderBehavior implements RenderBehavior {
		@Override
		public void render(float deltaTime) {
			renderWorld();
		}
	}

	@Override
	public void init() {
		worldRenderer.init();
		worldController.init();
	}
}
