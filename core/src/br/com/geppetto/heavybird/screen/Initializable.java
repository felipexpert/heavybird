package br.com.geppetto.heavybird.screen;

public interface Initializable {
	void init();
}
