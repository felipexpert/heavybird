package br.com.geppetto.heavybird.screen.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;

import br.com.geppetto.heavybird.ads.External;
import br.com.geppetto.heavybird.controller.Mode;
import br.com.geppetto.heavybird.controller.ModeManager;
import br.com.geppetto.heavybird.screen.DirectedGame;
import br.com.geppetto.heavybird.screen.GameScreen;
import br.com.geppetto.heavybird.screen.menu.MenuScreen;
import br.com.geppetto.heavybird.screen.transition.ScreenTransition;
import br.com.geppetto.heavybird.screen.transition.ScreenTransitionSlice;
import br.com.geppetto.heavybird.screen.transition.ScreenTransitionSlide;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;

public class ScreenManager {
	public static ScreenManager instance = new ScreenManager();
	
	private DirectedGame game;
	
	private ScreenManager(){}
	
	public void init(DirectedGame game) {
		this.game = game;
	}
	
	private void buildGame() {
		External.instance.handler.showFooterAds(false);
		SoundManager.instance.play(Sound.PLAY);
		GameScreen gameScreen = new GameScreen();
		ScreenTransition transition = ScreenTransitionSlide.init(.75f,
				ScreenTransitionSlide.DOWN, false, Interpolation.bounceOut);
		game.setScreen(gameScreen, transition);
		//Gdx.input.setCatchBackKey(true);
		Gdx.input.setInputProcessor(gameScreen.getInputProcessor());
	}
	
	public void setScreen(ScreenType screenType) {
		switch (screenType) {
		case GAME_HARDCORE:
			ModeManager.INSTANCE.currentMode = Mode.HARDCORE;
			buildGame();
			break;
		case GAME_EASY:
			ModeManager.INSTANCE.currentMode = Mode.EASY;
			buildGame();
			break;
		case MENU:
			External.instance.handler.showFullScreenAdMenu();
			MenuScreen menuScreen = new MenuScreen();
			ScreenTransition transition = ScreenTransitionSlice.init(1.5f, ScreenTransitionSlice.UP_DOWN, 4, Interpolation.bounce);
			//ScreenTransition transition = ScreenTransitionFade.init(.75f);
			game.setScreen(menuScreen, transition);
			//Gdx.input.setCatchBackKey(false);
			Gdx.input.setInputProcessor(menuScreen.getInputProcessor());
			External.instance.handler.showFooterAds(true);
			break;
		}
	}
}
