package br.com.geppetto.heavybird.screen.manager;

public enum ScreenType {
	MENU, GAME_EASY, GAME_HARDCORE;
}
