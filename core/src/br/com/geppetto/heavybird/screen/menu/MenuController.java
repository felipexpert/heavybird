package br.com.geppetto.heavybird.screen.menu;

import br.com.geppetto.heavybird.controller.handler.TouchInputHandler;
import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.gameObject.menuitem.MenuItem;
import br.com.geppetto.heavybird.renderer.GUIRenderable;
import br.com.geppetto.heavybird.screen.manager.ScreenManager;
import br.com.geppetto.heavybird.screen.manager.ScreenType;
import br.com.geppetto.heavybird.screen.menu.bird.BirdMenu;
import br.com.geppetto.heavybird.screen.menu.secret.CloudsSecret;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.command.ExitFromGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class MenuController implements GUIRenderable{
	private final GenericMenuItem easy;
	private final GenericMenuItem hardcore;
	private final TouchInputHandler handler;
	private final GenericMenuItem exit;
	private final BirdMenu birdMenu;
	private final GenericMenuItem logo;
	private final Array<AbstractGameObject> grounds = new Array<AbstractGameObject>();
	private final AbstractGameObject brown;
	
	private final Array<GenericMenuItem> itens;
	
	//private boolean quitClicked;
	//private float quitTime;
	
	private final ExitFromGame exitCommand = new ExitFromGame();
	
	//private static final float CLOUD_VELOCITY = -6f;
	//private final Array<AbstractGameObject> clouds = new Array<AbstractGameObject>();
	private final CloudsSecret cloudsSecrete;
	
	public MenuController() {
		logo = new GenericMenuItem(MenuItem.LOGO);
		easy = new GenericMenuItem(MenuItem.EASY);
		hardcore = new GenericMenuItem(MenuItem.HARDCORE);
		handler = new TouchInputHandler();
		exit = new GenericMenuItem(MenuItem.EXIT);
		float x = Constants.VIEWPORT_WIDTH / 2f;
		float y = Constants.VIEWPORT_HEIGHT / 2f;
		birdMenu = new BirdMenu(x + 3.25f, y, hardcore);
		hardcore.setCenterX(x - 1f);
		easy.setCenterX(x);
		easy.setCenterY(y + 4f);
		logo.setCenterX(x);
		logo.setY(Constants.VIEWPORT_HEIGHT - logo.getHeight());
		exit.setCenterX(x);
		exit.setY(y - 3f - exit.getHeight());
		cloudsSecrete = new CloudsSecret();
		
		{
			float width = 0f;
			
			while(width < Constants.VIEWPORT_WIDTH) {
				AbstractGameObject green = GameObjectFactory.instance.getGameObject(GameObject.GROUND_GREEN);
				green.updatePositionY(exit.getY() - green.getImgHeight());
				green.updatePositionX(width);
				width += green.getImgWidth();
				grounds.add(green);
			}
		}
		brown = GameObjectFactory.instance.getGameObject(GameObject.GROUND_BROWN);
		brown.setImgSize(Constants.VIEWPORT_WIDTH, grounds.first().getY());
		brown.updatePosition(0f, 0f);
		
		itens = new Array<GenericMenuItem>();
		itens.add(easy);
		itens.add(hardcore);
		itens.add(exit);
	}
	
	public void update(float delta) {
		exitCommand.update(delta);
		handler();
		birdMenu.update(delta);
		cloudsSecrete.update(delta);
	}
	/**
	 * Did the player chosed a button?
	 * @return
	 */
	private void handler() {
		GenericMenuItem choosed = handler.isMenuItemPressed(itens);
		if(choosed == easy) {
			ScreenManager.instance.setScreen(ScreenType.GAME_EASY);
		} else if(choosed == hardcore) {
			ScreenManager.instance.setScreen(ScreenType.GAME_HARDCORE);
		} else if(choosed == exit) {
			exitCommand.execute(null);
		}
	}

	@Override
	public void renderGUI(SpriteBatch batch) {
		cloudsSecrete.render(batch);
		logo.render(batch);
		easy.render(batch);
		hardcore.render(batch);
		birdMenu.render(batch);
		brown.render(batch);
		for(AbstractGameObject g : grounds) {
			g.render(batch);
		}
		exit.render(batch);
	}

	@Override
	public void resize(int width, int height) {
		handler.setScreenResolution(width, height);
		cloudsSecrete.resize(width, height);
	}
}
