package br.com.geppetto.heavybird.screen.menu.bird;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.heavybird.gameObject.AbstractGameObject;
import br.com.geppetto.heavybird.gameObject.GameObject;
import br.com.geppetto.heavybird.gameObject.GameObjectFactory;
import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;

public class BirdMenu {
	private final AbstractGameObject birdMenu = GameObjectFactory.instance.getGameObject(GameObject.BIRD_MENU);
	private final AbstractGameObject anchor = GameObjectFactory.instance.getGameObject(GameObject.ANCHOR);
	private float time;
	private boolean up = true;
	
	private static final float UP = .2f;
	private static final float DOWN = .4f;
	private static final float VELOCITY = 2.75f;
	
	private final float startingPosition;
	private final GenericMenuItem follower;
	
	public BirdMenu(float x, float y, GenericMenuItem follower) {
		this.startingPosition = y;
		this.follower = follower;
		birdMenu.updateCenterX(x);
		birdMenu.updateCenterY(y);
		anchor.updateCenterX(x);
		anchor.updatePositionY(y - anchor.getImgHeight());
	}
	
	public void update(float delta) {
		time += delta;
		birdMenu.updateAnimation(delta);
		if(time >= DOWN) {
			time = 0f;
			birdMenu.updateCenterY(startingPosition);
			up = true;
		} else if(time >= UP) {
			up = false;
			birdMenu.addPositionY(- delta * VELOCITY);
		} else {
			birdMenu.addPositionY((up ? 1f : -1f) * delta * VELOCITY);
		}
		follower.setCenterY(birdMenu.getCenter().y);
		anchor.updatePositionY(birdMenu.getY() - anchor.getImgHeight());
	}
	
	public void render(SpriteBatch batch) {
		birdMenu.render(batch);
		anchor.render(batch);
	}
}
