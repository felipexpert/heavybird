package br.com.geppetto.heavybird.screen.menu.secret;

import br.com.geppetto.heavybird.asset.Assets;
import br.com.geppetto.heavybird.controller.handler.TouchInputHandler;
import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.persistence.LocalPersistence;
import br.com.geppetto.heavybird.sound.Sound;
import br.com.geppetto.heavybird.sound.SoundManager;
import br.com.geppetto.heavybird.util.Constants;
import br.com.geppetto.heavybird.util.Digit.Digits;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class CloudsSecret {
	private static final String TAG = CloudsSecret.class.getSimpleName();
	private static final float padding = .0625f;
	private static final float width = 1f;
	private static final float height = 1.25f;
	private static final float x = 0f;
	private static final float y = Constants.VIEWPORT_HEIGHT / 2f;
	
	private static final float CLOUD_VELOCITY = -6f;
	
	private final TouchInputHandler handler;
	private final Array<CloudMenu> clouds;
	
	private final Digits timeDigit;
	
	private int next;
	
	private boolean firstTime;
	
	private static final float THEME_TIME = 47f;
	private float playingTime = 999f;
	private boolean displayNumber;
	
	public CloudsSecret() {
		clouds = new Array<CloudMenu>();
		this.handler = new TouchInputHandler();
		for(int i = 0; i < 7; i++)
			clouds.add(new CloudMenu());
		clouds.get(0).updatePosition(2f, 5.75f);
		clouds.get(1).updatePosition(13f, 5.75f);
		clouds.get(2).updatePosition(14f, 10f);
		clouds.get(3).updatePosition(3f, 10f);
		clouds.get(4).updatePosition(-1.5f, 17.125f);
		clouds.get(5).updatePosition(9f, 15f);
		clouds.get(6).updatePosition(3f, 13.5f);
		timeDigit = new Digits(Assets.instance.digits, 
				padding, width, height);
		timeDigit.updateByLeftDownCorner(0, x, y);
		firstTime = true;
	}
	
	public void render(SpriteBatch batch) {
		for(CloudMenu c : clouds) {
			c.render(batch);
		}
		if(displayNumber) {
			timeDigit.render(batch);
		}
	}
	
	public void update(float delta) {
		CloudMenu cloud = (CloudMenu) handler.isMenuItemPressed(clouds);
		playingTime += delta;
		if(playingTime <= THEME_TIME) {
			displayNumber = true;
			int number = (int) (THEME_TIME - playingTime);
			if(timeDigit.getNumber() != number) {
				timeDigit.updateByLeftDownCorner(number, x, y);
			}
		} else {
			displayNumber = false;
		}
		if(cloud != null) {
			if(firstTime) {
				arrangeCloudCodes(cloud); //this cloud will be zero
			} else {
				if(!cloud.isChoosed()) {
					if(cloud.getCode() == next) {
						next++;
						cloud.setChoosed(true);
						if(next == clouds.size) {
							if(playingTime > THEME_TIME) {
								Gdx.app.debug(TAG, "Vivaldi!");
								SoundManager.instance.playTheme();
								restartChallenge();
								playingTime = 0;
							} else {
								Gdx.app.debug(TAG, "New skins!");
								int sb = 0;
								int sa = 0;
								while(true) {
									sb = MathUtils.random(Constants.BIRD_SKINS - 1);
									sa = MathUtils.random(Constants.ANCHOR_SKINS - 1);
									if(sb != LocalPersistence.instance.getBirdSkin() ||
									   sa != LocalPersistence.instance.getAnchorSkin()) {
										break;
									}
								}
								LocalPersistence.instance.persistSkins(sb, sa);
								LocalPersistence.instance.loadSkin();
								Assets.instance.reloadSkins();
								newChallenge();
								SoundManager.instance.stopTheme();
								SoundManager.instance.play(Sound.POINT);
								playingTime = 999f;
							}
						}
					} else {
						restartChallenge(); 
					}
				}
			}
		}
		
		for(GenericMenuItem c : clouds) {
			c.addPositionX(CLOUD_VELOCITY * delta);
			if(c.getX() + c.getWidth() < 0f)
				c.updatePositionX(Constants.VIEWPORT_WIDTH);
		}
	}
	
	private void arrangeCloudCodes(CloudMenu cloud) {
		cloud.setCode(0);
		cloud.setChoosed(true);
		next++;
		Array<CloudMenu> undone = new Array<CloudMenu>();
		for(CloudMenu c : clouds) {
			undone.add(c);
		}
		undone.removeValue(cloud, true);
		int i = 1;
		while(undone.size > 0) {
			CloudMenu r = undone.random();
			r.setCode(i++);
			undone.removeValue(r, true);
		}
		firstTime = false;
	}
	
	private void restartChallenge() {
		next = 0;
		for(CloudMenu c : clouds) {
			c.setChoosed(false);
		}
	}
	
	private void newChallenge() {
		firstTime = true;
		next = 0;
		for(CloudMenu c : clouds) {
			c.reset();
		}
	}
	
	public void resize(int width, int height) {
		handler.setScreenResolution(width, height);
	}
}
