package br.com.geppetto.heavybird.screen.menu.secret;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import br.com.geppetto.heavybird.gameObject.menuitem.GenericMenuItem;
import br.com.geppetto.heavybird.gameObject.menuitem.MenuItem;

public class CloudMenu extends GenericMenuItem {
	private boolean choosed;
	private int code;
	
	public CloudMenu() {
		super(MenuItem.CLOUD_MENU);
	}
	
	public boolean isChoosed() {
		return choosed;
	}

	public void setChoosed(boolean choosed) {
		this.choosed = choosed;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public void render(SpriteBatch batch) {
		if(choosed)
			batch.setColor(1f, 1f, 1f, .5f);
		super.render(batch);
		batch.setColor(1f, 1f, 1f, 1f);
	}
	
	public void reset() {
		code = 0;
		choosed = false;
	}
}
