package br.com.geppetto.heavybird.screen.menu;

import br.com.geppetto.heavybird.controller.controller.WorldController;
import br.com.geppetto.heavybird.renderer.GUIRenderer;
import br.com.geppetto.heavybird.screen.AbstractGameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MenuScreen extends AbstractGameScreen {

	private final GUIRenderer guiRenderer;
	
	private final MenuController controller;
	
	private boolean paused;
	
	public MenuScreen() {
		this.controller = new MenuController();
		this.guiRenderer = new GUIRenderer();
	}
	
	@Override
	public void render(float deltaTime) {
		Gdx.gl.glClearColor(WorldController.clearGameColor.r,
				WorldController.clearGameColor.g,
				WorldController.clearGameColor.b,
				WorldController.clearGameColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(!paused) {
			controller.update(deltaTime);
		}
		guiRenderer.render();
	}

	@Override
	public void resize(int width, int height) {
		guiRenderer.resize(width, height);
	}

	@Override
	public void show() {
		init();
	}
	
	@Override
	public void resume() {
		paused = false;
		super.resume();
	}

	@Override
	public void hide() {
		guiRenderer.dispose();
	}

	@Override
	public void pause() {
		super.pause();
		paused = true;
	}

	@Override
	public InputProcessor getInputProcessor() {
		return null;
	}

	@Override
	public void init() {
		guiRenderer.init(new SpriteBatch(), controller);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		guiRenderer.dispose();
	}

}
